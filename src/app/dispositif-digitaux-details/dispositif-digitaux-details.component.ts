import { Title } from '@angular/platform-browser';
import { GlobalService } from './../global.service';
import { Component, OnInit, ViewChild, AfterViewChecked, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dispositif-digitaux-details',
  templateUrl: './dispositif-digitaux-details.component.html',
  styleUrls: ['./dispositif-digitaux-details.component.css']
})
export class DispositifDigitauxDetailsComponent implements OnInit {
  name: string;
  isFirst: boolean = false;
  isLast: boolean = false;
  imgprotrait : string ;
  language: string = this.globals.getlang();
  dispo : {} ;

  dispotifs = [{
    id : "patrimoine" ,
    title : "paterimoine.title" ,
    imgprotrait : "assets/modules/patrimoine/images/portrait.jpg",
    imgcoin : "assets/modules/patrimoine/images/emission-maitresse.png",
    imglegend : "paterimoine.legend",
    text : "paterimoine.desc",
  },
  {
    id : "billet-maton" ,
    title : "billet-maton.title" ,
    imgprotrait : "assets/modules/billet-maton/images/portrait.jpg",
    imgcoin : "assets/modules/billet-maton/images/emission-maitresse.png",
    imglegend : "billet-maton.legend",
    text : "billet-maton.desc",
  },
  {
    id : "salle-de-projection" ,
    title : "salle-de-projection.title" ,
    imgprotrait : "assets/modules/salle-de-projection/images/portrait.jpg",
    imgcoin : "assets/modules/salle-de-projection/images/emission-maitresse.png",
    imglegend : "salle-de-projection.legend",
    text : "salle-de-projection.desc",
  },{
    id : "bornes-des-jeux" ,
    title : "bornes-des-jeux.title" ,
    imgprotrait : "assets/modules/bornes-des-jeux/images/portrait.jpg",
    imgcoin : "assets/modules/bornes-des-jeux/images/emission-maitresse.png",
    imglegend : "bornes-des-jeux.legend",
    text : "bornes-des-jeux.desc",
  }] ;



  constructor(
    private globals: GlobalService,
    private router: Router,
    private route: ActivatedRoute,
    private titleService: Title
    ) {
      this.route.params.subscribe(params => {
        this.name = params.name;
        this.dispo = this.dispotifs.find(dispo => dispo.id == this.name );
        this.imgprotrait = this.dispo['imgprotrait'];

        if (this.name == "patrimoine") {
          this.isFirst = true;
          this.isLast = false;

        }if (this.name == "bornes-des-jeux") {
          this.isLast = true;
          this.isFirst = false;
        } if (this.name == "billet-maton" || this.name == "salle-de-projection"  ) {
          this.isLast = false;
          this.isFirst = false;
        }
      });
    }


    goprevious() {

      switch (this.name) {

        case "patrimoine":
        break;

        case "billet-maton":
        this.router.navigate(['dispositifs-digitaux/patrimoine']);
        break;

        case "salle-de-projection":
        this.router.navigate(['dispositifs-digitaux/billet-maton']);
        break;

        case "bornes-des-jeux":
        this.router.navigate(['dispositifs-digitaux/salle-de-projection']);
        break;

        default:
        break;
      }

    }

    gonext() {

      switch (this.name) {
        case "patrimoine":
        this.router.navigate(['dispositifs-digitaux/billet-maton']);
        break;

        case "billet-maton":
        this.router.navigate(['dispositifs-digitaux/salle-de-projection']);
        break;

        case "salle-de-projection":
        this.router.navigate(['dispositifs-digitaux/bornes-des-jeux']);
        break;

        case "bornes-des-jeux":
        break;

        default:
        break;

      }
    }

    ColorStyleFirst() {
      let style = {
        fill: this.isFirst ? '#6c757d' : '#fff',
        cursor: this.isFirst ? 'not-allowed' : 'pointer'
      };
      return style;
    }

    ColorStyleLast() {
      let style = {
        fill: this.isLast ? '#6c757d' : '#fff',
        cursor: this.isLast ? 'not-allowed' : 'pointer'
      };
      return style;
    }



    ngOnInit() {
    }

    ngOnDestroy() {
      this.globals.setCurrentSubModule(null);
      this.globals.setCurrentModule(null);
    }

    ngAfterViewChecked() {
      this.titleService.setTitle(document.getElementById('titleModule').textContent);
    }
  }
