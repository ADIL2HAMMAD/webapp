import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { GlobalService } from '../global.service';


@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit {

  language: string = this.globals.getlang(); 

  
  constructor(private globals : GlobalService, private titleService: Title) { }

  ngOnInit() {
    this.titleService.setTitle("404 Page introuvable");
  }

}
