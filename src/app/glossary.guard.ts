import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { GlossaryService } from './glossary.service';

@Injectable({
  providedIn: 'root'
})
export class GlossaryGuard implements CanActivate {

  constructor(private router: Router, private glossary : GlossaryService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {

      const letter: string  = route.params.letter ;
      const word : string  = route.params.word ;

      const path = state.root.firstChild.routeConfig.path;

      if (path === 'glossary/:letter') {
        if (this.glossary.GetGlossary()[letter.toUpperCase()] == undefined) {
          // Letter doesn't exist
          this.router.navigateByUrl('/404');
          return false;
        }
      }

      if (path === 'glossary/:letter/:word') {
        if (this.glossary.GetGlossary()[letter.toUpperCase()] == undefined) {
          // Letter doesn't exist
          this.router.navigateByUrl('/404');
          return false;
        }
        if (this.glossary.GetDefinitionByWord(word) == undefined) {
          // Word doesn't exist
          this.router.navigateByUrl('/404');
          return false;
        }

      }


      // Letter & word exist
      return true;
    }

  }
