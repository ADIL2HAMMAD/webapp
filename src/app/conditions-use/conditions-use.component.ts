import { Title } from '@angular/platform-browser';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { GlobalService } from './../global.service';
import { StatsService } from '../stats.service';

@Component({
  selector: 'app-conditions-use',
  templateUrl: './conditions-use.component.html',
  styleUrls: ['./conditions-use.component.css']
})
export class ConditionsUseComponent implements OnInit, OnDestroy {
  language: string = this.globals.getlang();
  constructor(private titleService: Title, private globals: GlobalService) {}

  ngOnInit() {
    this.titleService.setTitle('Use Conditions Page ');
    this.globals.setCurrentSection("CONDITIONS D'UTILISATION");
  }

  ngOnDestroy() {
    this.globals.setCurrentSection('');
  }
}
