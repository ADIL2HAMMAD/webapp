import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { GlobalService } from './../global.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-visite-libre',
  templateUrl: './visite-libre.component.html',
  styleUrls: ['./visite-libre.component.css']
})
export class VisiteLibreComponent implements OnInit {
  Modules: {};
  language: string = this.globals.getlang();

  constructor(
    private globals: GlobalService,
    private router: Router,
    private titleService: Title
  ) {}

  gotoModule(module: any) {
    module.visited = true;

    this.globals.setCurrentModule(module);

    if (module.havesub === true) {
      this.router.navigate(['/modules', module.id, 'sub-module']);
    } else {
      this.router.navigate(['/modules', module.id, 'module-details']);
    }
  }

  ngOnInit() {
    this.Modules = this.globals.GetModules();

    this.globals.setCurrentSubModule(null);
    this.globals.setCurrentModule(null);

    this.titleService.setTitle('Visite Libre');
  }
}
