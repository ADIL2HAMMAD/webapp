import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class DispotifsDigitauxGuard implements CanActivate {
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      const name : string  = route.params.name ;
      const path = state.root.firstChild.routeConfig.path;


      if (path === 'dispositifs-digitaux/:name') {
        if (!name.match(/^(patrimoine|billet-maton|salle-de-projection|bornes-des-jeux)$/)) {
                     // Dispotifs digitaux doesn't exist
          this.router.navigateByUrl('/404');
          return false;
        }
      }
      return true;
  }


  constructor(private router: Router){}


}



