import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlossaryService {


  constructor() {
    this.alphabets.forEach(element => {
      if (this.glossaire_letter[element.alphabet] != undefined) {
        element.haveDefinition = true ;
      }
    });
  }


  private glossaire_letter = {
    "A": [
      {
        "mots": "Affinage",
        "description": "Opération qui consiste à purifier les métaux pour atteindre un degré de pureté suffisant pour être utilisé dans plusieurs domaines.",
        "lettre": "A"
      },
      {
        "mots": "Ajustage",
        "description": "Opération qui consiste à ajuster les pièces de monnaies pour atteindre le poids, la dimension et l'épaisseur voulus.",
        "lettre": "A"
      },
      {
        "mots": "Alliage",
        "description": "Résultat de la combinaison par fusion d'un élément métallique avec un ou plusieurs autres métaux.",
        "lettre": "A"
      },
      {
        "mots": "Aloi (titre)",
        "description": "L’aloi de la monnaie, ou titre de la monnaie, désigne le rapport de la masse d’un métal fin à la masse totale de l’alliage qui compose la monnaie. En numismatique, l’aloi s’exprime en pourcentage.",
        "lettre": "A"
      },
      {
        "mots": "Altérer",
        "description": "Fait d’introduire un mélange de métaux à une monnaie, engendrant une modification du poids, du titre et de la qualité de la pièce.",
        "lettre": "A"
      },
      {
        "mots": "Alun",
        "description": "Sulfate hydraté d'aluminium et de potassium utilisé dans plusieurs domaines comme la médecine ou la peinture et utilisé aussi comme monnaie chez certaines civilisations de l’Egypte et de la Syrie comme du temps des Ayyoubides (1169-1229).",
        "lettre": "A"
      },
      {
        "mots": "Anépigraphe",
        "description": "Monnaie dépourvue de légendes ou d’inscriptions.",
        "lettre": "A"
      },
      {
        "mots": "Annelet",
        "description": "Petit anneau qui orne une monnaie.",
        "lettre": "A"
      },
      {
        "mots": "Anonyme",
        "description": "Qualificatif d’une monnaie qui ne porte aucune indication du pouvoir émetteur.",
        "lettre": "A"
      },
      {
        "mots": "Antoninien",
        "description": "Antoninianus, monnaie romaine créée par l’empereur Caracalla en 215. Frappée à l’origine en argent, l’antoninien fut progressivement dévalué pour devenir une monnaie en billon. Cette monnaie se distingue par la représentation de l’empereur coiffé d’une couronne radiée.",
        "lettre": "A"
      },
      {
        "mots": "Aureus",
        "description": "Monnaie romaine d’or équivalant à 25 deniers d’argent. Elle a été émise sporadiquement durant l’époque républicaine. Son émission devient régulière à l’époque impériale jusqu’au IVème siècle où elle est remplacée par le Solidus d’or.",
        "lettre": "A"
      },
      {
        "mots": "Autorité émettrice",
        "description": "Pouvoir responsable de l’émission d’une pièce de monnaie ou d’un billet de banque.",
        "lettre": "A"
      },
      {
        "mots": "Avers",
        "description": "Face d’une monnaie ou d’une médaille qui porte l’empreinte du coin inférieur. L’avers contient généralement le dessin ou l’inscription principale.",
        "lettre": "A"
      },
      {
        "mots": "Axe de coin",
        "description": "C’est l’orientation des coins utilisés pour effectuer la frappe monétaire. Cette orientation est exprimée en heures. Par exemple un axe de 6H exprime une orientation vers le bas du revers par rapport à l’avers.",
        "lettre": "A"
      }
    ],
    "B": [
      {
        "mots": "Balancier",
        "description": "Machine inventée au XVIème siècle en Italie et perfectionné à la même époque en Allemagne permettant la frappe mécanique des monnaies. La frappe au balancier ou bien au moulin n’a été entièrement adoptée dans les hôtels des monnaies en France qu’avec le règne de Louis XIV (1643-1715) et l'entière suppression du monnayage au marteau.",
        "lettre": "B"
      },
      {
        "mots": "Bêche",
        "description": "Pièces de monnaie ayant l’aspect d’une bêche (outil de labour) utilisées dans certaines dynasties chinoises et/ou africaines avant l’apparition et l’emploi des pièces rondes.",
        "lettre": "B"
      },
      {
        "mots": "Belle épreuve",
        "description": "Pièce monétaire qui se caractérise par un haut niveau de perfection, obtenu par l’utilisation de coins spécialement préparés et conçus à sa frappe. Cette monnaie est manipulée avec soin et conservée dans des conditions très favorables.",
        "lettre": "B"
      },
      {
        "mots": "Besant",
        "description": "Monnayage d’or frappé à Byzance et qui se caractérise par l’imitation des monnaies arabes et byzantines. Au Moyen âge, le mot besant fut donné à toutes sortes de pièces d'or.",
        "lettre": "B"
      },
      {
        "mots": "Billon",
        "description": "Alliage de cuivre et d’argent. Sa teneur en argent est variable mais elle dépasse souvent 50%. Le billon est aussi une monnaie divisionnaire de faible valeur.",
        "lettre": "B"
      },
      {
        "mots": "Bimétallisme",
        "description": "Système monétaire basé sur deux métaux le plus souvent l’or et l’argent. Ce système exige l’existence d’un rapport de valeur légal qui permet d'échanger l'un des métaux contre l'autre.",
        "lettre": "B"
      },
      {
        "mots": "Bon de monnaie",
        "description": "Billet marocain d’une valeur de 5, 20 et 50 francs émis en 1943 par les imprimeries de Casablanca.",
        "lettre": "B"
      },
      {
        "mots": "Bronze d’aluminium",
        "description": "Alliage composé de cuivre et d’aluminium avec un aspect doré. Ses propriétés particulières en l’occurrence la dureté et la résistance à la corrosion font de lui un choix idéal pour la frappe monétaire par rapport à d’autres alliages.",
        "lettre": "B"
      },
      {
        "mots": "Bronze de nickel",
        "description": "Alliage formé de plus de 70% de cuivre et de 25% de nickel.",
        "lettre": "B"
      },
      {
        "mots": "Bunduqi",
        "description": "Dinar ‘alaouite d’or de 3,50 g environ émis à partir de 1089 H. /1678 par ordre de Moulay Ismâ’îl (1672-1727). Cette monnaie d’or fut étalonnée sur le sequin de Venise en portant invariablement l’inscription suivante : « Dieu est Vérité, il fait triompher la vérité évidente ».",
        "lettre": "B"
      },
      {
        "mots": "Burin",
        "description": "Ciseau d’acier permettant de couper les métaux. Il est utilisé aussi par le graveur afin de rectifier et ajuster les imperfections d’une pièce de monnaie.",
        "lettre": "B"
      }
    ],
    "C": [
      {
        "mots": "Calligraphie",
        "description": "Etymologiquement, belle écriture ; Art de soigner l’exécution des caractères de l’écriture manuscrite la rendant élégante et parfois ornée.",
        "lettre": "C"
      },
      {
        "mots": "Cannelures",
        "description": "Stries parallèles qui ornementent la tranche d'une pièce de monnaie.",
        "lettre": "C"
      },
      {
        "mots": "Carat",
        "description": "Dérivant du mot grec « Keration », carat désigne un tiers d’obole et est une mesure de pureté pour les métaux précieux. Il désigne aussi la proportion d'or fin contenu par une monnaie. Il représente 1/24 de la masse totale d'un alliage.",
        "lettre": "C"
      },
      {
        "mots": "Cauris",
        "description": "Petits coquillages utilisés anciennement comme monnaie dans différentes civilisations à l’instar de celles de l’Asie et de l’Afrique. Actuellement, les cauris sont devenus des éléments décoratifs.",
        "lettre": "C"
      },
      {
        "mots": "Champ",
        "description": "Le champ d’une pièce est la surface du centre qui est délimitée par la légende circulaire.",
        "lettre": "C"
      },
      {
        "mots": "Coin",
        "description": "Outil cylindrique en métal, généralement en acier. Il porte à son extrémité l’image en creux des motifs qui seront imprimés en relief sur une face du flan monétaire.",
        "lettre": "C"
      },
      {
        "mots": "Commémorative",
        "description": "Une médaille ou une monnaie commémorative est une pièce émise dans le but de commémorer un événement de valeur spéciale. Ces monnaies sont destinées à des fins de collection et non à être mise en circulation commerciale.",
        "lettre": "C"
      },
      {
        "mots": "Contrefaçon",
        "description": "Fabrication de fausses monnaies généralement d’un poids et d’une valeur amoindris.",
        "lettre": "C"
      },
      {
        "mots": "Contremarque",
        "description": "Une marque estampillée ou gravée apposée sur une pièce de monnaie ayant déjà été en circulation. La contremarque est un signe utilisé pour étendre la validité de cette monnaie dans le temps.",
        "lettre": "C"
      },
      {
        "mots": "Coulée",
        "description": "Se dit d’une monnaie fondue dans un moule et non pas frappée au marteau. Le procédé du moulage fut adopté par plusieurs civilisations à l’instar des Grecs, des Romains et des Chinois.",
        "lettre": "C"
      },
      {
        "mots": "Coupure",
        "description": "Un billet de banque.",
        "lettre": "C"
      },
      {
        "mots": "Cours",
        "description": "Valeur fixée pour une monnaie en cours de circulation.",
        "lettre": "C"
      },
      {
        "mots": "Cours légal",
        "description": "Terme lié aux monnaies et aux billets en circulation qui répondent aux critères légaux fixés par la loi du pays.",
        "lettre": "C"
      },
      {
        "mots": "Créséides",
        "description": "Monnaies en or et en argent apparues au VIème siècle avant notre ère dans le royaume de Lydie. Elles présentent les protomés affrontés d’un lion et d’un taureau.",
        "lettre": "C"
      }
    ],
    "D": [
      {
        "mots": "Demi-follis",
        "description": "Monnaie romaine en bronze instaurée par l’Empereur Dioclétien qui a continué à être émise par les Byzantins et les Arabes.",
        "lettre": "D"
      },
      {
        "mots": "Démonétisation",
        "description": "Suppression de la valeur faciale d’une monnaie (pièce ou billet), la démonétisation fait perdre à la monnaie sa valeur et cette dernière ne peut plus servir à son usage prévu.",
        "lettre": "D"
      },
      {
        "mots": "Dénéral",
        "description": "Plaque ronde servant de moyen de contrôle des poids des monnaies émises.",
        "lettre": "D"
      },
      {
        "mots": "Denier",
        "description": "Monnaie d’argent qui fait partie du système monétaire romain. Le denier dérive du mot latin denarius (pluriel : denarii) qui signifie une dizaine. Cette unité monétaire, apparue sous la république romaine, a survécu après la chute de l’Empire romain jusqu’à l’époque médiévale.",
        "lettre": "D"
      },
      {
        "mots": "Dénomination",
        "description": "Désigne à la fois l’appellation et la valeur légale donnée à la monnaie par le pouvoir émetteur.",
        "lettre": "D"
      },
      {
        "mots": "Diadème",
        "description": "Bandeau décoré dont l’origine est hellénistique. Il est souvent porté par les Empereurs et les Rois comme un signe de pouvoir et de monarchie.",
        "lettre": "D"
      },
      {
        "mots": "Différent",
        "description": "Le différent monétaire est un signe ou marque gravée sur une monnaie afin de reconnaître les différents responsables de son émission : le nom de l’atelier, le nom de son directeur ou encore le nom du monnayeur.",
        "lettre": "D"
      },
      {
        "mots": "Dinar",
        "description": "Nom donné à l’unité monétaire d’or adoptée dans le monde musulman à l’époque médiévale. L’appellation dinar est tirée de \"denarius\", ancienne monnaie romaine. Les premiers dinars musulmans furent émis à l’époque du Calife omeyyade ‘Abd al-Malik ibn Marwân (685-705) vers la fin du VIIème siècle. Ce sont des imitations du monnayage byzantin. Au Maroc, le dinar est devenu la monnaie officielle des dynasties qui ont régné depuis les Almoravides jusqu’aux Saâdiens avec quelques émissions des premiers Rois ‘alaouites.",
        "lettre": "D"
      },
      {
        "mots": "Dirham",
        "description": "Cette unité monétaire trouve son origine dans le mot  grecque drachme. Elle était utilisée dans plusieurs cités-Etats helléniques. Les premières émissions musulmanes de cette monnaie ont été effectuées à l’époque du Calife omeyyade ‘Abd al-Malik ibn Marwân en 77 H. /697 ; elles s’inspiraient de la drachme sassanide. C’est ainsi que le nom drachme est transformé en dirham. Avec l’extension de l’Empire musulman vers l’Occident, cette monnaie fut frappée pour la première fois au Maroc par les Idrissides au VIIIème siècle.",
        "lettre": "D"
      },
      {
        "mots": "Dobla",
        "description": "Nom que portait le dinar almohade ya’qûbi en Andalousie. Cette pièce, d’un poids de 4,72 g, est l’équivalent de 2 écus d’or dont la frappe commence sous le règne d’Alphonse XI (1312-1350), Roi de Castille et de León en péninsule Ibérique.",
        "lettre": "D"
      },
      {
        "mots": "Double frappe",
        "description": "Ou tréflage, un glissement de flan lors de la frappe ce qui engendre le dédoublement de l’empreinte ou de la légende.",
        "lettre": "D"
      },
      {
        "mots": "Doublon",
        "description": "Monnaie espagnole en or qui a eu cours jusqu'au milieu du XIXème siècle. Pesant 6,77 grammes environ, le doublon est l’équivalent de 32 reales espagnols.",
        "lettre": "D"
      },
      {
        "mots": "Douro",
        "description": "Ancienne monnaie espagnole valant cinq pesetas.",
        "lettre": "D"
      },
      {
        "mots": "Drachme",
        "description": "Monnaie d’argent grecque émise par plusieurs villes grecques ainsi que par plusieurs civilisations du Proche-Orient comme les Sassanides. Le poids de la drachme dépend du système métrologique dans lequel elle s’inscrit. Ses multiples sont : le didrachme valant 2 drachmes, le tétradrachme ou statère valant 4 drachmes et le décadrachme valant 10 drachmes. Les divisions de la drachme sont l’hémidrachme ou triobole (1/2 drachme), le diobole (1/3 drachme), l’obole (1/6 drachme) et l’hémiobole (1/12 drachme).",
        "lettre": "D"
      },
      {
        "mots": "Ducat",
        "description": "Ou ducalis, une monnaie d’argent frappée à partir du XIIème siècle en Europe. Il y a eu de nombreux types de ducats mais le plus fameux était le ducat d’or frappé à Venise connu sous le nom de « Sequin ».",
        "lettre": "D"
      },
      {
        "mots": "Dupondius",
        "description": "Monnaie romaine en bronze qui fut introduite depuis l’époque républicaine. Sous le règne de l’Empereur Auguste (27 av. J.-C.-14 J.-C.), le dupondius fut frappé d’une façon régulière avec un alliage de cuivre doré proche du laiton.",
        "lettre": "D"
      }
    ],
    "E": [
      {
        "mots": "Ecu",
        "description": "Monnaie française en or et en argent créée au Moyen Âge et circulant avant la révolution. Cette monnaie fut frappée pour la première fois sous Louis IX en 1266 lors d’une réforme monétaire en portant une représentation des armoiries. Après la révolution, l’écu désignera une pièce en argent d’une valeur de 5 francs.",
        "lettre": "E"
      },
      {
        "mots": "Electrum",
        "description": "Alliage composé d’or et d’argent avec des proportions variables. Son utilisation dans le monnayage est apparue dans le Royaume de la Lydie (la Turquie actuelle) où des créséides en électrum furent frappées. Plusieurs civilisations se servaient de cet alliage pour la fabrication des bijoux et des armes comme les Egyptiens et les Indiens d’Amérique.",
        "lettre": "E"
      },
      {
        "mots": "Emblème",
        "description": "Toute représentation d’une valeur symbolique particulière.",
        "lettre": "E"
      },
      {
        "mots": "Emission",
        "description": "L’émission monétaire est le droit exclusif d’émettre des monnaies ou des billets de banque par l’institution émettrice. C’est aussi un ensemble de monnaies frappées ou de billets émis selon des caractéristiques physiques et typologiques identiques définies par l’autorité émettrice.",
        "lettre": "E"
      },
      {
        "mots": "Enclume",
        "description": "Objet métallique sur lequel on frappe les métaux. En numismatique c’est le coin dormant du monnayeur.",
        "lettre": "E"
      },
      {
        "mots": "Encre à couleur changeante",
        "description": "Encre qui change de couleur selon l’angle de vision.",
        "lettre": "E"
      },
      {
        "mots": "Entaille",
        "description": "Dommage fait sur le flan de la monnaie avec un instrument tranchant.",
        "lettre": "E"
      },
      {
        "mots": "Epreuve",
        "description": "Frappe spéciale effectuée en utilisant des coins et des flancs neufs avec un haut niveau de perfection.",
        "lettre": "E"
      },
      {
        "mots": "Ère de l’hégire",
        "description": "Ere musulmane qui commence avec le départ du Prophète Mohammed/Mahomet de la Mecque vers Médine, le 16 juillet 622. Cette date sert de début au comput du calendrier musulman fondé sur l’année lunaire qui compte 354 jours.",
        "lettre": "E"
      },
      {
        "mots": "Espèce",
        "description": "Désigne à la fois des billets et des pièces métalliques ayant un cours légal et qui peuvent être utilisés comme un moyen de paiement.",
        "lettre": "E"
      },
      {
        "mots": "Essai",
        "description": "La monnaie essai est une monnaie qui n’est pas mise en circulation malgré qu’elle soit identique à la monnaie courante. Un essai de monnaie peut être frappé sur un flan d’épaisseur normale ou double et avec divers métaux. La monnaie peut porter l’indication \"essai\" signifiant son utilité.",
        "lettre": "E"
      },
      {
        "mots": "Estampage",
        "description": "Opération qui consiste à imprimer en creux ou en relief sur le flan d’une monnaie une empreinte gravée sur une matrice ou un coin.",
        "lettre": "E"
      },
      {
        "mots": "Etalon",
        "description": "Valeur ou métal servant de référence pour une unité monétaire. L’étalon détermine la valeur de chaque espèce du système monétaire.",
        "lettre": "E"
      },
      {
        "mots": "Exergue",
        "description": "Petit espace, généralement de la face inférieure d’une monnaie ou d’une médaille, où on met souvent la date, le nom de l’atelier ou du graveur.",
        "lettre": "E"
      }
    ],
    "F": [
      {
        "mots": "Faible monnaie",
        "description": "Monnaie qui ne répond pas aux critères légaux notamment au niveau du poids et du titre, la faible monnaie a tendance à se déprécier vite avec le temps.",
        "lettre": "F"
      },
      {
        "mots": "Fausse pièce",
        "description": "Monnaie non authentique.",
        "lettre": "F"
      },
      {
        "mots": "Fels",
        "description": "Monnaie de cuivre apparue sous les Omeyyades vers la fin du VIIème siècle. Elle tire son nom du follis de cuivre frappé par les Romains et les Byzantins. Au Maroc, les premières frappes de fulûs effectuées par une dynastie locale sont faites à l’époque des Idrissides. Ces pièces contiennent souvent des inscriptions sur les deux faces. Leur frappe a continué jusqu’au XIXème siècle.",
        "lettre": "F"
      },
      {
        "mots": "Fil de sécurité",
        "description": "Dispositif de sécurité pour protéger le billet de banque contre la contrefaçon sous forme d’un mince ruban enfilé à travers le papier du billet.",
        "lettre": "F"
      },
      {
        "mots": "Filigrane",
        "description": "Motif ou dessin qui apparaît sur le billet de banque quand ce dernier est examiné face à la lumière.",
        "lettre": "F"
      },
      {
        "mots": "Flan",
        "description": "Rondelle de métal destinée à recevoir l'empreinte de la monnaie ou de la médaille.",
        "lettre": "F"
      },
      {
        "mots": "Fleur de coin",
        "description": "Description qui désigne le meilleur état de conservation d’une pièce qui ne présente aucune trace d’usure. La monnaie est dite frappée en fleur de coin quand il s’agit d’une pièce frappée avec un coin tout neuf et sur un flan présélectionné.",
        "lettre": "F"
      },
      {
        "mots": "Fleuron",
        "description": "Elément floral ornementant les pièces de monnaie.",
        "lettre": "F"
      },
      {
        "mots": "Follis",
        "description": "Monnaie romaine en bronze assimilée au Nummus. Le follis fut instauré en 294 J.-C. par l’Empereur Dioclétien lors de sa réforme monétaire.",
        "lettre": "F"
      },
      {
        "mots": "Frai",
        "description": "Usure de la monnaie par le frottement et la circulation, le frai peut engendrer une perte de poids du métal.",
        "lettre": "F"
      },
      {
        "mots": "Franc",
        "description": "Monnaie française apparue pour la première fois en 1360 sous le règne du Roi Jean II le Bon sous le nom du ‘Franc à cheval’ d’une valeur de 100 centimes. Le franc a continué à être émis en France et dans d’autres pays de l’Europe comme la Suisse et la Belgique, avant de disparaitre complètement en 2002 et de céder sa place à une nouvelle unité monétaire, l’Euro.",
        "lettre": "F"
      },
      {
        "mots": "Frappe",
        "description": "Action de frapper, au moyen d’une presse, un flan monétaire entre deux coins pour obtenir une monnaie. La frappe monétaire est toujours effectuée dans le cadre d’une émission décidée par un pouvoir émetteur pour répondre à des besoins financiers. Il existe trois techniques de frappe qui ont marqué l’histoire de l’émission monétaire : la frappe au marteau, la frappe au balancier et la frappe au levier.",
        "lettre": "F"
      },
      {
        "mots": "Frappe au balancier",
        "description": "Ou frappe au moulin, est une technique inventée au cours du XVIème siècle qui consiste à imprimer les coins monétaires sur le flan de la monnaie grâce à une force hydraulique qui remplace le marteau et l’enclume.",
        "lettre": "F"
      },
      {
        "mots": "Frappe au marteau",
        "description": "Méthode manuelle de fabrication des monnaies restée en utilisation jusqu’au début du XVIIème siècle. Cette opération consiste à mettre un flan vierge entre deux coins gravés en creux (un coin dormant et un autre mobile) afin d’imprimer les motifs à l’aide d’un marteau.",
        "lettre": "F"
      }
    ],
    "G": [
      {
        "mots": "Graveur",
        "description": "Personne ayant pour mission la conception et la réalisation des motifs des coins monétaires qui s’impriment au flan de la pièce.",
        "lettre": "G"
      },
      {
        "mots": "Grènetis",
        "description": "Cordon de petits grains en relief au bord de la pièce. Le grènetis entoure le contenu du champ d’une monnaie, d’une médaille ou d’un cachet.",
        "lettre": "G"
      },
      {
        "mots": "Gresham (loi de)",
        "description": "Loi financière posant le principe selon lequel « la mauvaise monnaie chasse la bonne », ce qui signifie que lorsqu’une monnaie est jugée comme bonne par le public, ce dernier a tendance à se débarrasser de la mauvaise monnaie et à thésauriser la bonne.",
        "lettre": "G"
      }
    ],
    "H": [
      {
        "mots": "Hémidrachme",
        "description": "Monnaie grecque équivalente à la moitié d’une drachme.",
        "lettre": "H"
      },
      {
        "mots": "Hexagramme",
        "description": "Grande monnaie d’argent byzantine émise essentiellement pendant le VIIème siècle. Elle fut créée par l’Empereur Héraclius en 615. L’hexagramme tire son nom de \"six grammata\" (6,84 g), cette monnaie est l’équivalent de ½ solidus d’or.",
        "lettre": "H"
      },
      {
        "mots": "Hôtel des monnaies",
        "description": "Bâtiment dédié à la frappe des médailles et des monnaies.",
        "lettre": "H"
      },
      {
        "mots": "Hybride",
        "description": "La monnaie hybride est celle qui est frappée avec deux coins différents qui n’étaient pas destinés à être utilisés ensemble. C’est une erreur d’émission.",
        "lettre": "H"
      }
    ],
    "I": [
      {
        "mots": "Iconographie monétaire",
        "description": "Ensemble d’images et de représentations qui apparait sur le champ de la monnaie. Il s’agit aussi d’une branche qui s’intéresse à l'identification, la description et l'interprétation du contenu de ces images.",
        "lettre": "I"
      },
      {
        "mots": "Imitation",
        "description": "L’imitation est une contrefaçon ou copie d’une monnaie officielle. Généralement de moindre qualité, l’imitation s’effectue souvent dans des circonstances de nécessité.",
        "lettre": "I"
      },
      {
        "mots": "Incuse",
        "description": "Se dit d’une monnaie qui présente le même motif de l’avers sur le revers mais en creux. Ce genre de monnaie fut frappé surtout en Grèce antique.",
        "lettre": "I"
      },
      {
        "mots": "Intrinsèque",
        "description": "En numismatique, le mot \"intrinsèque\" est relatif à la valeur inhérente de la monnaie. Cette valeur ne dépend pas de la valeur faciale que la monnaie peut porter, elle est plutôt déterminée par la nature, le poids et le titre du métal qu’elle contient.",
        "lettre": "I"
      }
    ],
    "J": [
      {
        "mots": "Jeton",
        "description": "Pièce qui prend la forme d’une monnaie, le jeton peut être réalisé en métal, en ivoire, en nacre etc. et servait autrefois pour calculer ou être donné comme cadeau. Le jeton est devenu aujourd’hui un outil pour payer au jeu ou pour faire fonctionner certains appareils.",
        "lettre": "J"
      }
    ],
    "L": [
      {
        "mots": "Lauré",
        "description": "Une effigie laurée est une figure couronnée de laurier.",
        "lettre": "L"
      },
      {
        "mots": "Légende",
        "description": "En numismatique, le mot \"légende\" se dit de toute inscription placée sur les monnaies, médailles, jetons etc. Les légendes peuvent se trouver à la fois sur l’avers et le revers de la pièce, ou encore sur la tranche. Elles peuvent être disposées circulairement ou en ligne droite.",
        "lettre": "L"
      },
      {
        "mots": "Lingots",
        "description": "Blocs de métaux non encore œuvré, et qui prennent généralement la forme du moule dans lequel ils ont été fondu.",
        "lettre": "L"
      },
      {
        "mots": "Listel",
        "description": "Le listel d’une monnaie est le rebord de la pièce. Il s’agit d’un cercle périphérique parfois pointillé entourant certaines pièces de monnaie afin d’en diminuer l’usure.",
        "lettre": "L"
      }
    ],
    "M": [
      {
        "mots": "Marabotin (Maravedi)",
        "description": "A l’époque almoravide, les dinars d’or frappés en Andalousie portaient le nom de \"Marabotin\". Ces monnaies ont eu cours dans la partie méridionale de la France au XIIème siècle. Le roi Alphonse VIII de Castille (1158-1214) a frappé des imitations de ces dinars qui portaient le nom de marabotins alfonsins.",
        "lettre": "M"
      },
      {
        "mots": "Marques d’ateliers",
        "description": "Motifs, lettres ou abréviations qui figurent sur les monnaies distinguant un atelier monétaire par rapport à un autre.",
        "lettre": "M"
      },
      {
        "mots": "Martelage",
        "description": "Action de détruire ou supprimer les images ou le nom de l’émetteur sur une monnaie. Cette opération est souvent pratiquée à des fins politiques en l’occurrence exprimer la déchéance d’un empereur ou d’un dirigeant.",
        "lettre": "M"
      },
      {
        "mots": "Matbu’",
        "description": "Monnaie d’or frappée pour la première fois en 1787 dans le cadre des réformes établies par le Sultan ‘alaouite Sidi Mohammed III (1757-1790). Cette monnaie de 16,9 g fut frappée à l’atelier de Madrid mais son émission n’avait pas eu de suites.",
        "lettre": "M"
      },
      {
        "mots": "Matrice de coin",
        "description": "En numismatique, la matrice se compose de deux coins en creux, un mobile et un autre dormant.",
        "lettre": "M"
      },
      {
        "mots": "Médaille",
        "description": "Pièce métallique frappée ou coulée sans valeur libératoire. Elle est émise pour commémorer une action, un événement ou pour honorer un personnage ou une institution.",
        "lettre": "M"
      },
      {
        "mots": "Mentions obligatoires",
        "description": "Exigence légale ou article de la loi contre la contrefaçon ou la falsification.",
        "lettre": "M"
      },
      {
        "mots": "Métrologie",
        "description": "Science qui s’intéresse à l’analyse des poids et des mesures de la monnaie.",
        "lettre": "M"
      },
      {
        "mots": "Micro texte",
        "description": "Petits caractères visibles à l’aide d’une loupe, imprimés sur plusieurs zones du billet de banque.",
        "lettre": "M"
      },
      {
        "mots": "Millarès",
        "description": "Le Millarès est l’imitation du qirat almohade qui fut instauré par le Sultan Abû Yûsuf Ya’qûb al-Mansour (1184-1199). Cette monnaie fut émise par le roi d’Aragon Jacques Ier (1213-1276) à partir de 1263. Elle a été également frappée à Pise et Gênes en Italie et à Agde en France.",
        "lettre": "M"
      },
      {
        "mots": "Millésime",
        "description": "Ensemble de chiffres désignant la date de frappe de la monnaie.",
        "lettre": "M"
      },
      {
        "mots": "Mithqâl",
        "description": "Unité de mesure de masse dans le monde musulman. Le mithqâl est aussi une monnaie ‘alaouite d’argent instaurée par Mohammed III (1757-1790) en 1775 d’un poids de 28,39 g. Sa valeur approximative est de 10 dirhams légaux.",
        "lettre": "M"
      },
      {
        "mots": "Module",
        "description": "Diamètre de la monnaie.",
        "lettre": "M"
      },
      {
        "mots": "Monétaire",
        "description": "Responsable de la frappe monétaire, son nom est parfois inscrit sur les monnaies comme chez les Mérovingiens de France.",
        "lettre": "M"
      },
      {
        "mots": "Monétarisation",
        "description": "Analyse des structures monétaires, leur évolution et leur mouvement avec le temps.",
        "lettre": "M"
      },
      {
        "mots": "Monétiforme",
        "description": "Terme désignant un objet dont la forme et l’aspect général ressemble à une pièce de monnaie.",
        "lettre": "M"
      },
      {
        "mots": "Monétique",
        "description": "Ensemble des moyens électroniques et des procédures informatiques utilisés afin d’assurer les transactions bancaires à l’instar des guichets électroniques et des distributeurs automatiques.",
        "lettre": "M"
      },
      {
        "mots": "Monétisation",
        "description": "Procédure établie à la fois par les banques centrales et commerciales  et qui consiste à introduire de nouvelles formes de moyens de paiement et de signes monétaires afin d’augmenter les liquidités disponibles.",
        "lettre": "M"
      },
      {
        "mots": "Monnaie",
        "description": "Ensemble d’instruments physiques ou virtuels permettant de mesurer la valeur et échanger des biens contre des services.",
        "lettre": "M"
      },
      {
        "mots": "Monnaie coulée",
        "description": "Monnaie produite à travers la fonte du métal dans un moule contenant la représentation gravée du contenu de la pièce (effigie, dessin, inscription etc.). Cette technique de coulage a été utilisée par plusieurs pouvoirs émetteurs comme les Grecques, les Etrusques et les Romains.",
        "lettre": "M"
      },
      {
        "mots": "Monnaie de collection",
        "description": "Monnaie présentant un intérêt numismatique qui se manifeste par  la rareté, l’ancienneté ou l’authenticité.",
        "lettre": "M"
      },
      {
        "mots": "Monnaie de compte",
        "description": "Monnaie inventée pour faciliter les comptes mais qui n’a jamais existé en espèces réelles.",
        "lettre": "M"
      },
      {
        "mots": "Monnaie de nécessité",
        "description": "Emission qui complète temporairement la monnaie officielle (pièce et billet). La monnaie de nécessité est émise généralement au cours des périodes de troubles économiques.",
        "lettre": "M"
      },
      {
        "mots": "Monnaie fiduciaire",
        "description": "Monnaie qui comprend les pièces et les billets de banque. Sa valeur n’est pas déterminée par son coût de production, elle est plutôt relative à la confiance que lui accorde ses utilisateurs.",
        "lettre": "M"
      },
      {
        "mots": "Monométallisme",
        "description": "Système monétaire fondé sur la reconnaissance d’un seul étalon monétaire en l’occurrence l’or, l’argent, le bronze ou encore d’autres métaux plus vil.",
        "lettre": "M"
      },
      {
        "mots": "Motif de repérage",
        "description": "Eléments incomplets spécifiant la valeur du billet de banque et qui se complètent, sous l’effet de la lumière, pour constituer la totalité de la valeur.",
        "lettre": "M"
      },
      {
        "mots": "Motif pour malvoyants",
        "description": "Motif imprimé en taille-douce avec un relief palpable par le toucher permettant aux malvoyants d’identifier le billet.",
        "lettre": "M"
      },
      {
        "mots": "Motif principal",
        "description": "Thématique ou dessin principal figurant sur le recto ou le verso d’un billet de banque.",
        "lettre": "M"
      },
      {
        "mots": "Mouzouna",
        "description": "Ou blanquille dans la terminologie française. Il s’agit d’une unité monétaire d’argent d’un poids de 1,16 g qui fut émise par Moulay ar-Rachîd (1664-1672) en 1669. L’émission de cette monnaie a continué sous le règne des Sultans ‘alaouites Moulay Ismâ’îl (1672-1727), Sidi Mohammed III (1757-1790), Moulay Sulaymân (1792-1822) et Moulay ‘Abd al-‘Azîz (1894-1908) lequel a réduit le poids de la mouzouna à un 1 g.",
        "lettre": "M"
      },
      {
        "mots": "Mumini",
        "description": "Dinar almohade instauré par le calife ‘Abd al-Mûmin ibn ‘Alî (1130-1163). Cette pièce, d’un poids de 2,34 g, était le résultat de la première réforme monétaire almohade.",
        "lettre": "M"
      }
    ],
    "N": [
      {
        "mots": "Nomisma",
        "description": "Nom que portait la monnaie dans le monde grec. Le nom de nomisma apparait à l’époque byzantine comme monnaie héritière du solidus romain.",
        "lettre": "N"
      },
      {
        "mots": "Numismatique",
        "description": "Numisma en latin :  science qui traite de la description et l’histoire de la monnaie. Cette discipline vient au service de l’histoire et de l’archéologie grâce aux importants indices de datation qu’elle fournit.",
        "lettre": "N"
      },
      {
        "mots": "Nummus",
        "description": "Nom latin qui signifie pièce de monnaie. Il s’agit essentiellement d’une pièce de monnaie en cuivre de faible valeur émise dans l’Antiquité par les Romains et les Byzantins.",
        "lettre": "N"
      }
    ],
    "P": [
      {
        "mots": "Papier-monnaie",
        "description": "Synonyme de la monnaie fiduciaire désignant les monnaies en papier comme les billets de banque que l’on ne peut pas convertir en métal précieux.",
        "lettre": "P"
      },
      {
        "mots": "Peseta",
        "description": "Ancienne unité monétaire principale de l’Espagne de 1868 à 2002, date à laquelle elle fut remplacée par l’Euro. La peseta était aussi la monnaie principale de la Guinée équatoriale.",
        "lettre": "P"
      },
      {
        "mots": "Pied-fort",
        "description": "Pièce métallique qu’on frappe pour servir de modèle, elle se caractérise par une épaisseur plus grande que les pièces de monnaies ordinaires.",
        "lettre": "P"
      },
      {
        "mots": "Poinçon",
        "description": "Instrument métallique souvent en acier contenant l’entière gravure de la monnaie et qui sert à l’enfonçage des coins de fabrication.",
        "lettre": "P"
      },
      {
        "mots": "Punique",
        "description": "Terme relatif à la civilisation carthaginoise, à ses colonies, à son écriture et à son monnayage. L’appellation siculo-punique fut associée au monnayage punique émis en Sicile.",
        "lettre": "P"
      }
    ],
    "Q": [
      {
        "mots": "Qirat",
        "description": "Monnaie d’argent de forme carrée, d’un poids de 1,5 g. émise au Maroc pour la première fois sous le règne du calife almohade Abû Yûsuf Ya’qûb al-Mansour (1184-1199). Sa frappe se poursuivra jusqu’à l’époque des Saâdiens.",
        "lettre": "Q"
      }
    ],
    "R": [
      {
        "mots": "Raison sociale",
        "description": "Désignation, d’un nom ou d’une signature figurant sur un billet et permettant l’identification des responsables de l’émission monétaire.",
        "lettre": "R"
      },
      {
        "mots": "Rare",
        "description": "Une monnaie est rare lorsqu’elle n’apparait pas fréquemment comme les monnaies communes de son époque.  Il peut s’agir d’un monogramme ou d’une variante.",
        "lettre": "R"
      },
      {
        "mots": "Real",
        "description": "Monnaie d’argent espagnole qui fut mise en circulation du XIVème siècle jusqu’au milieu du XIXème siècle.",
        "lettre": "R"
      },
      {
        "mots": "Recto",
        "description": "Face du billet de banque qui contient généralement le motif principal du billet.",
        "lettre": "R"
      },
      {
        "mots": "Real de ocho",
        "description": "Appelé également \"pièce de huit\" ou piastre, le real de ocho est une monnaie d’argent qui a été émise en Espagne à partir de la fin du XVIème siècle. Cette pièce était largement utilisée en tant qu’unité de compte internationale pour les transactions commerciales grâce à son caractère uniforme. La pièce de huit est la monnaie à partir de laquelle le dollar américain a été établi.",
        "lettre": "R"
      },
      {
        "mots": "Revers",
        "description": "Face opposée de l’avers d’une pièce ou d’une médaille.",
        "lettre": "R"
      },
      {
        "mots": "Rial Hassani",
        "description": "Monnaie ‘alaouite d’argent de 27 g environ créée par le Sultan Moulay al-Hassan Ier (1873-1894) en 1882. Elle s’inspire du real espagnol mais son poids était équivalent au mithqâl. L’émission du rial Hassani fut réalisée à Paris et sa frappe s’est poursuivie jusqu’au règne de Moulay ‘Abd al-‘Azîz (1894-1908).",
        "lettre": "R"
      }
    ],
    "S": [
      {
        "mots": "Semis",
        "description": "Monnaie romaine en bronze créée à l’époque républicaine, elle valait la moitié d’un as de bronze. Son émission suspendue au Ier siècle fut reprise au IVème siècle. Le semis désigna dès lors la moitié du solidus d’or puis du nomisma byzantin.",
        "lettre": "S"
      },
      {
        "mots": "Sequin",
        "description": "Appelé aussi \"ducat\", le sequin est une monnaie d’or émise pour la première fois à Venise à partir du XIIIème siècle. Sa valeur approximative est de 12 francs. Le sequin se considérait comme une monnaie de référence dans les grandes places commerçantes du bassin méditerranéen.",
        "lettre": "S"
      },
      {
        "mots": "Sesterce",
        "description": "Du latin \"sestertius\" qui signifie deux et demi. C’est une ancienne monnaie romaine en argent et de petites dimensions à l’époque républicaine avant qu’elle ne devienne en bronze à l’époque impériale avec une taille plus grande.",
        "lettre": "S"
      },
      {
        "mots": "Signatures",
        "description": "Sur le billet de banque, les signatures sont les garantes de l’authenticité du billet émanant des représentants de l’autorité émettrice.",
        "lettre": "S"
      },
      {
        "mots": "Signe monétaire",
        "description": "Terme désignant tout moyen physique servant de paiement quel que soit sa nature ou le matériau dont il est fabriqué.",
        "lettre": "S"
      },
      {
        "mots": "Solidus",
        "description": "Monnaie romaine de 4,51 g qui a été créée par Constantin Ier en 310 pour financer son armée et ses guerres civiles. Son émission a continué tout au long des IVème et Vème siècles.",
        "lettre": "S"
      },
      {
        "mots": "Sou",
        "description": "Nom porté par différentes monnaies de compte ou de règlement depuis l’Antiquité. A cette époque, le \"sou\" est la somme équivalant à 12 deniers ou 1/20ème de la livre de monnaie de compte.",
        "lettre": "S"
      },
      {
        "mots": "Spécimen",
        "description": "Exemplaire d’une monnaie ou d’un billet destiné aux instituts d’émission étrangers pour information. La mention \"spécimen\" peut être imprimée, perforée ou en surcharge.",
        "lettre": "S"
      },
      {
        "mots": "Statère",
        "description": "Terme générique désignant des monnaies antiques d’électrum, d’or et d’argent dans les divers systèmes monétaires du monde méditerranéen antique.",
        "lettre": "S"
      },
      {
        "mots": "Surfrappe",
        "description": "Action d’établir une seconde frappe sur une empreinte déjà existante sur une monnaie.",
        "lettre": "S"
      }
    ],
    "T": [
      {
        "mots": "Tétradrachme",
        "description": "Monnaie d’argent grecque émise par plusieurs villes grecques ainsi que par plusieurs civilisations du proche Orient comme celle des Sassanides.",
        "lettre": "T"
      },
      {
        "mots": "Titulature",
        "description": "Ensemble des titres que portent les souverains. La titulature est souvent mentionnée sur les monnaies musulmanes.",
        "lettre": "T"
      },
      {
        "mots": "Toghra",
        "description": "Marque monétaire qui caractérise certaines monnaies. Généralement connue chez les Ottomans, la Toghra est réalisée avec une calligraphie ornementale et fut utilisée comme marque de validation sur les monnaies du Sultan Saâdien Zaydân (1603-1628).",
        "lettre": "T"
      },
      {
        "mots": "Tranche",
        "description": "La tranche désigne le bord d’une pièce. Elle prend une forme lisse, cannelée ou inscrite.",
        "lettre": "T"
      },
      {
        "mots": "Tranche cannelée",
        "description": "Tranche qui porte des rainures parallèles verticales ou cannelures.",
        "lettre": "T"
      },
      {
        "mots": "Tranche inscrite",
        "description": "Tranche qui porte un texte gravé en creux ou imprimé en relief.",
        "lettre": "T"
      },
      {
        "mots": "Tréflage",
        "description": "Dans la frappe monétaire au marteau, le tréflage est une empreinte produite en double. C’est un défaut de frappe.",
        "lettre": "T"
      },
      {
        "mots": "Trousseau",
        "description": "Coin du dessus, opposé à la pile ou bien au coin dormant.",
        "lettre": "T"
      },
      {
        "mots": "Type monétaire",
        "description": "Ensemble des éléments iconographiques et épigraphiques qui caractérisent l’avers et le revers d’une monnaie.",
        "lettre": "T"
      },
      {
        "mots": "Typographie",
        "description": "Numéro de série du billet imprimé en encre spéciale.",
        "lettre": "T"
      }
    ],
    "U": [
      {
        "mots": "Uniface",
        "description": "Pièce (monnaie, médaille, jeton) frappée sur une face seulement.",
        "lettre": "U"
      },
      {
        "mots": "Uqiya",
        "description": "Unité pondérale marocaine qui correspond à 4,689 g ou 4 mouzounas.",
        "lettre": "U"
      }
    ],
    "V": [
      {
        "mots": "Valeur",
        "description": "Cours de la monnaie dans une période donnée.",
        "lettre": "V"
      },
      {
        "mots": "Valeur faciale",
        "description": "La valeur faciale d’une monnaie correspond à la valeur donnée par convention à la pièce au moment de son émission par le pouvoir émetteur. Cette valeur est généralement inscrite sur le revers de la pièce.",
        "lettre": "V"
      },
      {
        "mots": "Valeur légale",
        "description": "En numismatique, la valeur légale d’une monnaie est la valeur déterminée par les pouvoirs en place et les Etats selon le système monétaire en vigueur.",
        "lettre": "V"
      },
      {
        "mots": "Verso",
        "description": "Face opposée du recto.",
        "lettre": "V"
      },
      {
        "mots": "Virole",
        "description": "La virole est un élément mécanique qui sert dans la frappe des monnaies. C’est une sorte d’anneau dans lequel est placé le flan avant la frappe pour éviter l’écrasement de ce dernier. La virole contient les motifs qui vont être gravés sur la tranche de la monnaie.",
        "lettre": "V"
      }
    ],
    "Y": [
      {
        "mots": "Ya’qûbi",
        "description": "Monnaie d’or almohade créée par le calife Abû Yûsuf Ya’qûb al-Mansour (1184-1199) d’un poids de 4,72 g. L’instauration de ce dinar d’or s’inscrivait dans le cadre de la réforme monétaire qui a eu lieu en 581 H. /1185.",
        "lettre": "Y"
      }
    ]
  };


  private alphabets = [
    {
      alphabet : "A" ,
      haveDefinition : false
    },
    {
      alphabet : "B" ,
      haveDefinition : false
    },
    {
      alphabet : "C" ,
      haveDefinition : false
    },
    {
      alphabet : "D" ,
      haveDefinition : false
    },
    {
      alphabet : "E" ,
      haveDefinition : false
    },
    {
      alphabet : "F" ,
      haveDefinition : false
    },
    {
      alphabet : "G" ,
      haveDefinition : false
    },
    {
      alphabet : "H" ,
      haveDefinition : false
    },
    {
      alphabet : "I" ,
      haveDefinition : false
    },
    {
      alphabet : "J" ,
      haveDefinition : false
    },
    {
      alphabet : "K" ,
      haveDefinition : false
    },
    {
      alphabet : "L" ,
      haveDefinition : false
    },
    {
      alphabet : "M" ,
      haveDefinition : false
    },
    {
      alphabet : "N" ,
      haveDefinition : false
    },
    {
      alphabet : "O" ,
      haveDefinition : false
    },
    {
      alphabet : "P" ,
      haveDefinition : false
    },
    {
      alphabet : "Q" ,
      haveDefinition : false
    },
    {
      alphabet : "R" ,
      haveDefinition : false
    },
    {
      alphabet : "S" ,
      haveDefinition : false
    },
    {
      alphabet : "T" ,
      haveDefinition : false
    },
    {
      alphabet : "U" ,
      haveDefinition : false
    },
    {
      alphabet : "V" ,
      haveDefinition : false
    },
    {
      alphabet : "W" ,
      haveDefinition : false
    },
    {
      alphabet : "X" ,
      haveDefinition : false
    },
    {
      alphabet : "Y" ,
      haveDefinition : false
    },
    {
      alphabet : "Z" ,
      haveDefinition : false
    },] ;


    GetAlphabets(){
      return this.alphabets ;
    }

    GetGlossary(){
      return this.glossaire_letter ;
    }

    GetGlossaryByLetter(letter : string){
      return this.glossaire_letter[letter.toUpperCase()] ;
    }


    GetDefinitionByWord(word : string ){
      const letter = word.substr(0,1).toUpperCase() ;
      const allWords : [{mots: string, description: string, lettre: string}] = this.glossaire_letter[letter] || [] ;
      const searchedWord = allWords.find(value => value.mots.toUpperCase().includes(word.toUpperCase()));
      return  searchedWord ? searchedWord.description : undefined;

    }



    FilterByWord(letter : string , word : string ){
      const allWords : [{mots: string, description: string, lettre: string}] = this.glossaire_letter[letter.toUpperCase()];

      if (!allWords) {
        return;
      }

      const searchedWords = allWords.filter(value => value.mots.toUpperCase().includes(word.toUpperCase()));
      return  searchedWords;
    }


  }
