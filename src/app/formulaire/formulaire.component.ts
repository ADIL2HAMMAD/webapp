import { Router } from "@angular/router";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { NgForm } from "@angular/forms";
import { Title } from "@angular/platform-browser";

import { GlobalService } from "../global.service";
import { environment } from "../../environments/environment";

@Component({
  selector: "app-formulaire",
  templateUrl: "./formulaire.component.html",
  styleUrls: ["./formulaire.component.css"]
})
export class FormulaireComponent implements OnInit, OnDestroy {
  language: string = this.globals.getlang();
  successMessage: string;

  subscription = {
    fullname: "",
    email: "",
    agree: false
  };

  constructor(
    private http: HttpClient,
    private globals: GlobalService,
    private router: Router,
    private titleService: Title
  ) {}

  subscribe(subscribeForm: NgForm) {
    console.log(subscribeForm);
    console.log(this.subscription);

    // The form is invalid, so no need to proceed
    if (subscribeForm.invalid) {
      return false;
    }

    // Ignore in development
    if (!environment.production) {
      subscribeForm.resetForm();
      this.subscription = {
        fullname: "",
        email: "",
        agree: false
      };

      this.successMessage = "formulaire.success";
      setTimeout(() => this.router.navigateByUrl("/thanks"), 2500);
      return false;
    }

    const headers = new HttpHeaders();
    headers.set("Content-Type", "text/plain; charset=utf-8");

    this.http
      .post("/newsletter/subscribe", this.subscription, {
        headers,
        responseType: "text"
      })
      .subscribe({
        next: () => {
          subscribeForm.resetForm();
          this.subscription = {
            fullname: "",
            email: "",
            agree: false
          };

          this.successMessage = "formulaire.success";
          setTimeout(() => this.router.navigateByUrl("/thanks"), 2500);
        },
        error: error => console.error(error)
      });
  }

  ngOnInit() {
    this.titleService.setTitle("Formulaire Page ");

    this.globals.setCurrentSection("NEWSLETTER");
  }

  ngOnDestroy() {
    this.globals.setCurrentSection("");
  }
}
