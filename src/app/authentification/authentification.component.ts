import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';

import { GlobalService } from '../global.service';
import { WsScanService } from '../ws-scan.service';

@Component({
  selector: 'app-authentification',
  templateUrl: './authentification.component.html',
  styleUrls: ['./authentification.component.css']
})
export class AuthentificationComponent implements OnInit {
  isempty = false;
  isinvalid = false;

  constructor(
    private globals: GlobalService,
    private scanService: WsScanService,
    private titleService: Title
  ) {}

  language: string = this.globals.getlang();

  validation(code: any) {
    if (code === '') {
      this.isempty = true;
    } else if (code.length === 10 && /^[a-zA-Z0-9]{10}$/.test(code)) {
      const subscription = this.scanService.responses.subscribe(response => {
        if (response === 'valid') {
          this.isinvalid = false;
          this.globals.startSession('INPUT', code);
        } else if (response === 'invalid') {
          this.isinvalid = true;
        }

        subscription.unsubscribe();
      });

      this.scanService.emitScan(code);
    } else {
      this.isinvalid = true;
    }
  }

  ngOnInit() {
    this.titleService.setTitle('Authentification Page ');
  }
}
