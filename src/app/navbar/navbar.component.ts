import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  constructor(private globals: GlobalService, private router: Router) {}

  isToggle: boolean = null;
  language: string = this.globals.getlang();

  toggle() {
    this.isToggle = !this.isToggle; 
  }
 

  ngOnInit() {}
}
