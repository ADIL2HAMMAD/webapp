import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { GlobalService } from './global.service';
import { PreviousRouteService } from './previous-route.service';
import { SessionService } from './session.service';
import { StatsService } from './stats.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Bank Al Maghrib Webapp';

  constructor(
    private router: Router,
    private global: GlobalService,
    private previousRouteService: PreviousRouteService,
    private sessionService: SessionService,
    private statsService: StatsService
  ) {
    const TWO_HOURS_IN_MILLISECONDS = 10 * 60 * 1000; // 2 * 60 * 60 * 1000;

    // Destroy it if the session is older than 2 hours
    if (
      new Date().getTime() - this.sessionService.getLastActive().getTime() >
      TWO_HOURS_IN_MILLISECONDS
    ) {
      this.closeSession();
    }

    // Check the session every 10 seconds and destroy it if it's older than 2 hours
    setInterval(() => {
      const now = new Date().getTime();

      if (now - this.sessionService.getLastActive().getTime() > TWO_HOURS_IN_MILLISECONDS) {
        this.closeSession();
      }
    }, 10 * 1000);

    document.addEventListener(
      'touchstart',
      () => {
        if (this.sessionService.isLoggedIn()) {
          this.sessionService.setLastActive(new Date());
        }
      },
      false
    );
  }

  closeSession() {
    this.global.setCurrentSubModule(null);
    this.global.setCurrentModule(null);
    this.global.setCurrentVisitType('');
    this.global.setCurrentSection('');

    this.statsService.endSession();
    this.router.navigateByUrl('/');
  }

  ngOnInit() {
     window.addEventListener("load", function() { window. scrollTo(0, 1); });
  }
}
