import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { GlobalService } from './global.service';
import { WsScanService } from './ws-scan.service';

@Injectable({
  providedIn: 'root'
})
export class IdentifyCanActivate implements CanActivate {
  constructor(
    private router: Router,
    private scanService: WsScanService,
    private globals: GlobalService
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const code = route.params.code;

    if (code && code.length === 10 && /^[a-zA-Z0-9]{10}$/.test(code)) {
      const subscription = this.scanService.responses.subscribe(response => {
        if (response === 'valid') {
          this.globals.startSession('URL', code);
        } else if (response === 'invalid') {
          this.router.navigateByUrl('/404');
        }

        subscription.unsubscribe();
      });

      this.scanService.emitScan(code);
      return true;
    }

    this.router.navigateByUrl('/404');
    return false;
  }
}
