import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { GlossaryService } from 'src/app/glossary.service';
import { Router , ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-glossary-letter-words',
  templateUrl: './glossary-letter-words.component.html',
  styleUrls: ['./glossary-letter-words.component.css']
})
export class GlossaryLetterWordsComponent implements OnInit {

  letter : string ;
  LetterWords : any ;
  IsLetterWords : boolean = false ;
  SearchedWord : string ;
  constructor(
    private titleService: Title,
    private router: Router ,
    private glossary : GlossaryService ,
    private route : ActivatedRoute) {    }


    GoToWordDefinition(word : string){
    this.router.navigate(['glossary', this.letter, word ]);
    }

    search(){
      this.LetterWords = Object.entries(this.glossary.FilterByWord(this.letter , this.SearchedWord))
      if (!this.LetterWords || this.LetterWords.length <= 0 ) {
        this.IsLetterWords = true ;
      }
      console.log(this.IsLetterWords);

    }




    ngOnInit() {
      this.titleService.setTitle('Glossary Letter Words List');
      this.letter = this.route.snapshot.paramMap.get("letter");
      this.LetterWords =  Object.entries(this.glossary.GetGlossaryByLetter(this.letter)) ;
      console.log(this.IsLetterWords);
    }


  }
