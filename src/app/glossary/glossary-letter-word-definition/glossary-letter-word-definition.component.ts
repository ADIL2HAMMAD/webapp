import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { GlossaryService } from 'src/app/glossary.service';
import { Router , ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-glossary-letter-word-definition',
  templateUrl: './glossary-letter-word-definition.component.html',
  styleUrls: ['./glossary-letter-word-definition.component.css']
})
export class GlossaryLetterWordDefinitionComponent implements OnInit {


  definition : string ;
  LetterWords : any ;
  word :string ;
  constructor(
    private titleService: Title,
    private router: Router ,
    private glossary : GlossaryService ,
    private route : ActivatedRoute) {    }



  ngOnInit() {
    this.titleService.setTitle('Glossary Letter Words Definition');
    const letter = this.route.snapshot.paramMap.get("letter");
    this.word = this.route.snapshot.paramMap.get("word");
    this.definition = this.glossary.GetDefinitionByWord(this.word);
  }


  BackToGlossry(){
    this.router.navigate(['/glossary'])
  }

  BackToWordList(){
    window.history.back();
  }

}
