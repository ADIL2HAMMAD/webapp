import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { GlobalService } from '../../global.service';
import { GlossaryService } from '../../glossary.service';

@Component({
  selector: 'app-glossary-letter-list',
  templateUrl: './glossary-letter-list.component.html',
  styleUrls: ['./glossary-letter-list.component.css']
})
export class GlossaryLetterListComponent implements OnInit, OnDestroy {
  language: string = this.globals.getlang();
  alphabetsData = [];

  constructor(
    private titleService: Title,
    private globals: GlobalService,
    private glossary: GlossaryService,
    private route: Router
  ) {
    this.alphabetsData = this.glossary.GetAlphabets();
  }

  goToWords(letterClicked: any) {
    console.log(letterClicked);
    if (letterClicked.haveDefinition) {
      this.route.navigate(['/glossary/', letterClicked.alphabet.toUpperCase()]);
    }
  }

  ngOnInit() {
    this.titleService.setTitle('Glossary Letter List');

    this.globals.setCurrentSection('GLOSSAIRE');
  }

  ngOnDestroy() {
    this.globals.setCurrentSection('');
  }
}
