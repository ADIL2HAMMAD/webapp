import { Title } from '@angular/platform-browser';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { GlobalService } from '../global.service';

@Component({
  selector: 'app-dispositifs',
  templateUrl: './dispositifs.component.html',
  styleUrls: ['./dispositifs.component.css']
})
export class DispositifsComponent implements OnInit, OnDestroy {
  language: string = this.globals.getlang();

  constructor(private titleService: Title, private globals: GlobalService) {}

  ngOnInit() {
    this.titleService.setTitle('DISPOSITIFS DIGITAUX LIST');

    this.globals.setCurrentSection("DISPOSITIFS DIGITAUX LIST");
  }

  ngOnDestroy() {
    this.globals.setCurrentSection('');
  }
}
