import { Title } from '@angular/platform-browser';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { GlobalService } from './../global.service';

@Component({
  selector: 'app-conseils-visiteurs',
  templateUrl: './conseils-visiteurs.component.html',
  styleUrls: ['./conseils-visiteurs.component.css']
})
export class ConseilsVisiteursComponent implements OnInit, OnDestroy {
  constructor(private titleService: Title, private globals: GlobalService) {}

  language: string = this.globals.getlang();

  ngOnInit() {
    this.titleService.setTitle('Tips Visitors Page ');

    this.globals.setCurrentSection('CONSEILS POUR LES VISITEURS');
  }

  ngOnDestroy() {
    this.globals.setCurrentSection('');
  }
}
