import { DispositifDigitauxDetailsComponent } from './dispositif-digitaux-details/dispositif-digitaux-details.component';
import { GlossaryLetterWordDefinitionComponent } from './glossary/glossary-letter-word-definition/glossary-letter-word-definition.component';
import { GlossaryLetterWordsComponent } from './glossary/glossary-letter-words/glossary-letter-words.component';
import { GlossaryLetterListComponent } from './glossary/glossary-letter-list/glossary-letter-list.component';
import { VisiteLibreComponent } from './visite-libre/visite-libre.component';
import { PlanMuseeComponent } from './plan-musee/plan-musee.component';
import { DispositifsComponent } from './Dispositifs/dispositifs.component';
import { ConseilsVisiteursComponent } from './conseils-visiteurs/conseils-visiteurs.component';
import { ConditionsUseComponent } from './conditions-use/conditions-use.component';
import { ContactusComponent } from './contactus/contactus.component';
import { AuthentificationComponent } from './authentification/authentification.component';
import { SelectVisiteComponent } from './select-visite/select-visite.component';
import { FormulaireComponent } from './formulaire/formulaire.component';
import { SelectLanguageComponent } from './select-language/select-language.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { VisiteGuideeComponent } from './visite-guidee/visite-guidee.component';
import { ModuleCanActivate } from './module-guard.service';
import { NotFoundComponent } from './not-found/not-found.component';
import { ThanksComponent } from './thanks/thanks.component';

import { QrcodeIdentificationScannerComponent } from './qrcode-identification-scanner/qrcode-identification-scanner.component';
import { QrcodeCollectionScannerComponent } from './qrcode-collection-scanner/qrcode-collection-scanner.component';
import { IdentifyCanActivate } from './identify.service';
import { AuthGuard } from './auth-guard.service';
import { NoAuthGuard } from './no-auth-guard.service';
import { GlossaryGuard } from './glossary.guard';
import { DispotifsDigitauxGuard } from './dispotifs-digitaux.guard';

const routes: Routes = [
  {
    path: 'glossary',
    component: GlossaryLetterListComponent,
    canActivate: [GlossaryGuard]

  },
  {
    path: 'glossary/:letter',
    component: GlossaryLetterWordsComponent,
    canActivate: [GlossaryGuard]

  },
  {
    path: 'glossary/:letter/:word',
    component: GlossaryLetterWordDefinitionComponent,
    canActivate: [GlossaryGuard]

  },
  {
    path: '',
    component: SelectLanguageComponent,
    canActivate: [NoAuthGuard]
  },
  {
    path: 'change-language',
    component: SelectLanguageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'welcome',
    component: WelcomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'formulaire',
    component: FormulaireComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'selectvisite',
    component: SelectVisiteComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'auth',
    component: AuthentificationComponent,
    canActivate: [NoAuthGuard]
  },
  {
    path: 'contactus',
    component: ContactusComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'conditions-generale',
    component: ConditionsUseComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'conseils-visiteurs',
    component: ConseilsVisiteursComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'dispositifs-digitaux',
    component: DispositifsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'dispositifs-digitaux/:name',
    component: DispositifDigitauxDetailsComponent,
    canActivate: [DispotifsDigitauxGuard]
  },
  {
    path: 'musee-plan',
    component: PlanMuseeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'visite-guidee/:order',
    component: VisiteGuideeComponent,
    canActivate: [AuthGuard, ModuleCanActivate]
  },
  {
    path: 'modules',
    component: VisiteLibreComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'qrcodeidentification',
    component: QrcodeIdentificationScannerComponent,
    canActivate: [NoAuthGuard]
  },
  {
    path: 'qrcodecollection',
    component: QrcodeCollectionScannerComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '404',
    component: NotFoundComponent
  },
  {
    path: 'thanks',
    component: ThanksComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'identify/:code',
    canActivate: [IdentifyCanActivate],
    // We don't use this component as this is not a page. This just so that
    // Angular can't complain about not providing a component :D
    component: WelcomeComponent
  },
  {
    path: '**',
    redirectTo: '404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [ModuleCanActivate]
})
export class AppRoutingModule {}
