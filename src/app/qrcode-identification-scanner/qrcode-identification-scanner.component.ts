import { Title } from '@angular/platform-browser';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { QrcodeReaderComponent } from '../qrcode-reader/qrcode-reader.component';
import { WsScanService } from '../ws-scan.service';
import { GlobalService } from '../global.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-qrcode-identification-scanner',
  templateUrl: './qrcode-identification-scanner.component.html',
  styleUrls: ['./qrcode-identification-scanner.component.css']
})
export class QrcodeIdentificationScannerComponent implements OnInit {
  @ViewChild(QrcodeReaderComponent)
  private qrcodeReader: QrcodeReaderComponent;

  constructor(
    private globals: GlobalService,
    private router: Router,
    private scanService: WsScanService,
    private titleService: Title,
    private location: Location
  ) {}

  ngOnInit() {
    this.titleService.setTitle('Qr Code Scan Page');
  }

  back(){
    this.location.back();
  }

  send(code) {
    const subscription = this.scanService.responses.subscribe(response => {
      if (response === 'valid') {
        this.globals.startSession('SCAN', code);
      } else if (response === 'invalid') {
        this.qrcodeReader.reload();
      }

      subscription.unsubscribe();
    });

    this.scanService.emitScan(code);
  }
}
