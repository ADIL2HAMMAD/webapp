import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { StatsService } from './stats.service';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  mylang: string = 'fr';


  map = {
    aPo6boZuti : {idModule : 1},
    BzmKjPa7jt : {idModule : 2},
    eIr01BFc9R : {idModule : 3},
    eniY8qwVti : {idModule : 5},
    f4hXBMNFgY : {idModule : 8},
    fxk9Eds1YY : {idModule : 9},
    HGYcjN3FPQ : {idModule : 12},
    HtPGvR7l5B : {idModule : 14},
    JutFI9TYDK : {idModule : 17},
    M44YsVRzA1 : {idModule : 20},
    mJ3RqFwhXW : {idModule : 22},
    mvbLg1hFyK : {idModule : 23},
    No47ZDOf1H : {idModule : 25},
    QCAuQOQ2bw : {idModule : 27},
    QjZpGVdVZX : {idModule : 28},
    Rhke3jauAJ : {idModule : 30},
    tHhvfbRfko : {idModule : 31},
    wYfaJ2Gt4h : {idModule : 32},
    y8IpQdegrN : {idModule : 33},
    YfAPHAXazt : {idModule : 34},

  }
  private Modules =[
    {
      id : "1",
      imgprotrait:'assets/modules/1/images/img_portrait_'+this.getlang()+'.png',
      imgcoins:{},
      audio:{
        ar:'assets/modules/1/audio/ar_sound.mp3',
        fr:'assets/modules/1/audio/fr_sound.mp3',
        en:'assets/modules/1/audio/en_sound.mp3',
        es:'assets/modules/1/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:true
    },
    {
      id : "2",
      imgprotrait:'assets/modules/2/images/img_portrait.png',
      imgcoins:{},
      audio:{
        ar:'assets/modules/2/audio/ar_sound.mp3',
        fr:'assets/modules/2/audio/fr_sound.mp3',
        en:'assets/modules/2/audio/en_sound.mp3',
        es:'assets/modules/2/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:true
    },
    {
      id : "3",
      imgprotrait:'assets/modules/3/images/img_portrait.png',
      imgcoins:{
        "1":'assets/modules/3/images/coin1.png',
        "2":'assets/modules/3/images/coin2.png',
        "3":'assets/modules/3/images/coin3.png',
        "4":'assets/modules/3/images/coin4.png',
        "5":'assets/modules/3/images/coin5.png'
      },
      audio:{
        ar:'assets/modules/3/audio/ar_sound.mp3',
        fr:'assets/modules/3/audio/fr_sound.mp3',
        en:'assets/modules/3/audio/en_sound.mp3',
        es:'assets/modules/3/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "4",
      imgprotrait:'assets/modules/4/images/img_portrait.png',
      imgcoins:{
        "1":'assets/modules/4/images/coin1.png'
      },
      audio:{
        ar:'assets/modules/4/audio/ar_sound.mp3',
        fr:'assets/modules/4/audio/fr_sound.mp3',
        en:'assets/modules/4/audio/en_sound.mp3',
        es:'assets/modules/4/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "5",
      imgprotrait:'assets/modules/5/images/img_portrait.png',
      imgcoins:{
        "1":'assets/modules/5/images/coin1.png',
        "2":'assets/modules/5/images/coin2.png',
        "3":'assets/modules/5/images/coin3.png',
      },
      audio:{
        ar:'assets/modules/5/audio/ar_sound.mp3',
        fr:'assets/modules/5/audio/fr_sound.mp3',
        en:'assets/modules/5/audio/en_sound.mp3',
        es:'assets/modules/5/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "6",
      imgprotrait:'assets/modules/6/images/img_portrait.png',
      imgcoins:{},
      audio:{
        ar:'assets/modules/6/audio/ar_sound.mp3',
        fr:'assets/modules/6/audio/fr_sound.mp3',
        en:'assets/modules/6/audio/en_sound.mp3',
        es:'assets/modules/6/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "7",
      imgprotrait:'assets/modules/7/images/img_portrait.png',
      imgcoins:{
        "1":'assets/modules/7/images/coin1.png',
        "2":'assets/modules/7/images/coin2.png',
      },
      audio:{
        ar:'assets/modules/7/audio/ar_sound.mp3',
        fr:'assets/modules/7/audio/fr_sound.mp3',
        en:'assets/modules/7/audio/en_sound.mp3',
        es:'assets/modules/7/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "8",
      imgprotrait:'assets/modules/8/images/img_portrait.png',
      imgcoins:{},
      audio:{
        ar:'assets/modules/8/audio/ar_sound.mp3',
        fr:'assets/modules/8/audio/fr_sound.mp3',
        en:'assets/modules/8/audio/en_sound.mp3',
        es:'assets/modules/8/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "9",
      imgprotrait:'assets/modules/9/images/img_portrait.png',
      imgcoins:{
        "1":'assets/modules/9/images/coin1.png',
        "2":'assets/modules/9/images/coin2.png'
      },
      audio:{
        ar:'assets/modules/9/audio/ar_sound.mp3',
        fr:'assets/modules/9/audio/fr_sound.mp3',
        en:'assets/modules/9/audio/en_sound.mp3',
        es:'assets/modules/9/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "10",
      imgprotrait:'assets/modules/10/images/img_portrait.png',
      imgcoins:{},
      audio:{
        ar:'assets/modules/10/audio/ar_sound.mp3',
        fr:'assets/modules/10/audio/fr_sound.mp3',
        en:'assets/modules/10/audio/en_sound.mp3',
        es:'assets/modules/10/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "11",
      imgprotrait:'assets/modules/11/images/img_portrait.png',
      imgcoins:{
        "1":'assets/modules/11/images/coin1.png',
        "2":'assets/modules/11/images/coin2.png',
        "3":'assets/modules/11/images/coin3.png',
        "4":'assets/modules/11/images/coin4.png',
        "5":'assets/modules/11/images/coin5.png'
      },
      audio:{
        ar:'assets/modules/11/audio/ar_sound.mp3',
        fr:'assets/modules/11/audio/fr_sound.mp3',
        en:'assets/modules/11/audio/en_sound.mp3',
        es:'assets/modules/11/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "12",
      imgprotrait:'assets/modules/12/images/img_portrait.png',
      imgcoins:{
        "1":'assets/modules/12/images/coin1.png',
        "2":'assets/modules/12/images/coin2.png',
        "3":'assets/modules/12/images/coin3.png',
      },
      audio:{
        ar:'assets/modules/12/audio/ar_sound.mp3',
        fr:'assets/modules/12/audio/fr_sound.mp3',
        en:'assets/modules/12/audio/en_sound.mp3',
        es:'assets/modules/12/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "13",
      imgprotrait:'assets/modules/13/images/img_portrait.png',
      imgcoins:{
        "1":'assets/modules/13/images/coin1.png'
      },
      audio:{
        ar:'assets/modules/13/audio/ar_sound.mp3',
        fr:'assets/modules/13/audio/fr_sound.mp3',
        en:'assets/modules/13/audio/en_sound.mp3',
        es:'assets/modules/13/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "14",
      imgprotrait:'assets/modules/14/images/img_portrait.png',
      imgcoins:{
        "1":'assets/modules/14/images/coin1.png',
        "2":'assets/modules/14/images/coin2.png',
      },
      audio:{
        ar:'assets/modules/14/audio/ar_sound.mp3',
        fr:'assets/modules/14/audio/fr_sound.mp3',
        en:'assets/modules/14/audio/en_sound.mp3',
        es:'assets/modules/14/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "15",
      imgprotrait:'assets/modules/15/images/img_portrait.png',
      imgcoins:{},
      audio:{
        ar:'assets/modules/15/audio/ar_sound.mp3',
        fr:'assets/modules/15/audio/fr_sound.mp3',
        en:'assets/modules/15/audio/en_sound.mp3',
        es:'assets/modules/15/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "16",
      imgprotrait:'assets/modules/16/images/img_portrait.png',
      imgcoins:{
        "1":'assets/modules/16/images/coin1.png',
        "2":'assets/modules/16/images/coin2.png'
      },
      audio:{
        ar:'assets/modules/16/audio/ar_sound.mp3',
        fr:'assets/modules/16/audio/fr_sound.mp3',
        en:'assets/modules/16/audio/en_sound.mp3',
        es:'assets/modules/16/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "17",
      imgprotrait:'assets/modules/17/images/img_portrait.png',
      imgcoins:{
        "1":'assets/modules/17/images/coin1.png',
        "2":'assets/modules/17/images/coin2.png',
        "3":'assets/modules/17/images/coin3.png',
      },
      audio:{
        ar:'assets/modules/17/audio/ar_sound.mp3',
        fr:'assets/modules/17/audio/fr_sound.mp3',
        en:'assets/modules/17/audio/en_sound.mp3',
        es:'assets/modules/17/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "18",
      imgprotrait:'assets/modules/18/images/img_portrait.png',
      imgcoins:{},
      audio:{
        ar:'assets/modules/18/audio/ar_sound.mp3',
        fr:'assets/modules/18/audio/fr_sound.mp3',
        en:'assets/modules/18/audio/en_sound.mp3',
        es:'assets/modules/18/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "19",
      imgprotrait:'assets/modules/19/images/img_portrait.png',
      imgcoins:{
        "1":'assets/modules/19/images/coin1.png',
        "2":'assets/modules/19/images/coin2.png'
      },
      audio:{
        ar:'assets/modules/19/audio/ar_sound.mp3',
        fr:'assets/modules/19/audio/fr_sound.mp3',
        en:'assets/modules/19/audio/en_sound.mp3',
        es:'assets/modules/19/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "20",
      imgprotrait:'assets/modules/20/images/img_portrait.png',
      imgcoins:{
        "1":'assets/modules/20/images/coin1.png',
        "2":'assets/modules/20/images/coin2.png',
        "3":'assets/modules/20/images/coin3.png',
      },
      audio:{
        ar:'assets/modules/20/audio/ar_sound.mp3',
        fr:'assets/modules/20/audio/fr_sound.mp3',
        en:'assets/modules/20/audio/en_sound.mp3',
        es:'assets/modules/20/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "21",
      imgprotrait:'assets/modules/21/images/img_portrait.png',
      imgcoins:{
        "1":'assets/modules/21/images/coin1.png'
      },
      audio:{
        ar:'assets/modules/21/audio/ar_sound.mp3',
        fr:'assets/modules/21/audio/fr_sound.mp3',
        en:'assets/modules/21/audio/en_sound.mp3',
        es:'assets/modules/21/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:true
    },
    {
      id : "22",
      imgprotrait:'assets/modules/22/images/img_portrait.png',
      imgcoins:{},
      audio:{
        ar:'assets/modules/22/audio/ar_sound.mp3',
        fr:'assets/modules/22/audio/fr_sound.mp3',
        en:'assets/modules/22/audio/en_sound.mp3',
        es:'assets/modules/22/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "23",
      imgprotrait:'assets/modules/23/images/img_portrait.png',
      imgcoins:{
        "1":'assets/modules/23/images/coin1.png',
        "2":'assets/modules/23/images/coin2.png',
        "3":'assets/modules/23/images/coin3.png',
      },
      audio:{
        ar:'assets/modules/23/audio/ar_sound.mp3',
        fr:'assets/modules/23/audio/fr_sound.mp3',
        en:'assets/modules/23/audio/en_sound.mp3',
        es:'assets/modules/23/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "24",
      imgprotrait:'assets/modules/24/images/img_portrait.png',
      imgcoins:{
        "1":'assets/modules/24/images/coin1.png' ,
      },
      audio:{
        ar:'assets/modules/24/audio/ar_sound.mp3',
        fr:'assets/modules/24/audio/fr_sound.mp3',
        en:'assets/modules/24/audio/en_sound.mp3',
        es:'assets/modules/24/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:true
    },
    {
      id : "25",
      imgprotrait:'assets/modules/25/images/img_portrait.png',
      imgcoins:{
        "1":'assets/modules/25/images/coin1.png',
        "2":'assets/modules/25/images/coin2.png',
        "3":'assets/modules/25/images/coin3.png',
      },
      audio:{
        ar:'assets/modules/25/audio/ar_sound.mp3',
        fr:'assets/modules/25/audio/fr_sound.mp3',
        en:'assets/modules/25/audio/en_sound.mp3',
        es:'assets/modules/25/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "26",
      imgprotrait:'assets/modules/26/images/img_portrait.png',
      imgcoins:{
        "1":'assets/modules/26/images/coin1.png'
      },
      audio:{
        ar:'assets/modules/26/audio/ar_sound.mp3',
        fr:'assets/modules/26/audio/fr_sound.mp3',
        en:'assets/modules/26/audio/en_sound.mp3',
        es:'assets/modules/26/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:true
    },
    {
      id : "27",
      imgprotrait:'assets/modules/27/images/img_portrait.png',
      imgcoins:{},
      audio:{
        ar:'assets/modules/27/audio/ar_sound.mp3',
        fr:'assets/modules/27/audio/fr_sound.mp3',
        en:'assets/modules/27/audio/en_sound.mp3',
        es:'assets/modules/27/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "28",
      imgprotrait:'assets/modules/28/images/img_portrait.png',
      imgcoins:{
        "1":'assets/modules/28/images/billet1.png',
        "2":'assets/modules/28/images/coin1.png',
      },
      audio:{
        ar:'assets/modules/28/audio/ar_sound.mp3',
        fr:'assets/modules/28/audio/fr_sound.mp3',
        en:'assets/modules/28/audio/en_sound.mp3',
        es:'assets/modules/28/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:true
    },
    {
      id : "29",
      imgprotrait:'assets/modules/29/images/img_portrait.png',
      imgcoins:{},
      audio:{
        ar:'assets/modules/29/audio/ar_sound.mp3',
        fr:'assets/modules/29/audio/fr_sound.mp3',
        en:'assets/modules/29/audio/en_sound.mp3',
        es:'assets/modules/29/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:true
    },
    {
      id : "30",
      imgprotrait:'assets/modules/30/images/img_portrait.png',
      imgcoins:{
        "1":'assets/modules/30/images/coin1.png',
        "2":'assets/modules/30/images/coin2.png',
        "3":'assets/modules/30/images/coin3.png',
        "4":'assets/modules/30/images/billet1.png',
        "5":'assets/modules/30/images/billet2.png',
        "6":'assets/modules/30/images/billet3.png',
        "7":'assets/modules/30/images/billet4.png',
      },
      audio:{
        ar:'assets/modules/30/audio/ar_sound.mp3',
        fr:'assets/modules/30/audio/fr_sound.mp3',
        en:'assets/modules/30/audio/en_sound.mp3',
        es:'assets/modules/30/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "31",
      imgprotrait:'assets/modules/31/images/img_portrait.png',
      imgcoins:{
        "1":'assets/modules/31/images/coin1.png',
        "2":'assets/modules/31/images/coin2.png',
        "3":'assets/modules/31/images/billet2.png',
        "4":'assets/modules/31/images/billet1.png',
      },
      audio:{
        ar:'assets/modules/31/audio/ar_sound.mp3',
        fr:'assets/modules/31/audio/fr_sound.mp3',
        en:'assets/modules/31/audio/en_sound.mp3',
        es:'assets/modules/31/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "32",
      imgprotrait:'assets/modules/32/images/img_portrait.png',
      imgcoins:{
        "1":'assets/modules/32/images/coin1.png'
      },
      audio:{
        ar:'assets/modules/32/audio/ar_sound.mp3',
        fr:'assets/modules/32/audio/fr_sound.mp3',
        en:'assets/modules/32/audio/en_sound.mp3',
        es:'assets/modules/32/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "33",
      imgprotrait:'assets/modules/33/images/img_portrait.png',
      imgcoins:{},
      audio:{
        ar:'assets/modules/33/audio/ar_sound.mp3',
        fr:'assets/modules/33/audio/fr_sound.mp3',
        en:'assets/modules/33/audio/en_sound.mp3',
        es:'assets/modules/33/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    },
    {
      id : "34",
      imgprotrait:'assets/modules/34/images/img_portrait.png',
      imgcoins:{},
      audio:{
        ar:'assets/modules/34/audio/ar_sound.mp3',
        fr:'assets/modules/34/audio/fr_sound.mp3',
        en:'assets/modules/34/audio/en_sound.mp3',
        es:'assets/modules/34/audio/es_sound.mp3'
      },
      visited:false,
      havecomplement:false
    }
  ];

  ModulesWithAudio = [
    { order: 1, element: this.GetModule(1) },
    { order: 2, element: this.GetModule(2) },

    { order: 3, element: this.GetModule(3) },
    { order: 4, element: this.GetModule(4) },

    { order: 5, element: this.GetModule(5) },
    { order: 6, element: this.GetModule(6) },
    { order: 7, element: this.GetModule(7) },
    { order: 8, element: this.GetModule(8) },

    { order: 9, element: this.GetModule(9) },
    { order: 10, element: this.GetModule(10) },
    { order: 11, element: this.GetModule(11) },

    { order: 12, element: this.GetModule(12) },
    { order: 13, element: this.GetModule(13) },

    { order: 14, element: this.GetModule(14) },
    { order: 15, element: this.GetModule(15) },
    { order: 16, element: this.GetModule(16) },

    { order: 17, element: this.GetModule(17) },
    { order: 18, element: this.GetModule(18) },
    { order: 19, element: this.GetModule(19) },

    { order: 20, element: this.GetModule(20) },
    { order: 21, element: this.GetModule(21) },

    { order: 22, element: this.GetModule(22) },

    { order: 23, element: this.GetModule(23) },
    { order: 24, element: this.GetModule(24) },

    { order: 25, element: this.GetModule(25) },
    { order: 26, element: this.GetModule(26) },
    { order: 27, element: this.GetModule(27) },

    { order: 28, element: this.GetModule(28) },
    { order: 29, element: this.GetModule(29) },

    { order: 30, element: this.GetModule(30) },
    { order: 31, element: this.GetModule(31) },

    { order: 32, element: this.GetModule(32) },
    { order: 33, element: this.GetModule(33) },
    { order: 34, element: this.GetModule(34) }


  ];

  private currentAudio: any;
  private currentModule: any;
  private currentSection: string;
  private currentSubModule: any;
  private currentVisit: string;

  constructor(private translate: TranslateService, private statsService: StatsService) {
    translate.setDefaultLang(this.mylang);
    if (this.statsService.iSessionStarted()) {
      this.statsService.sendStat({ name: 'SELECT_LANG', value: this.mylang, type: 'text' });
    }
  }

  setlang(languge: string) {
    this.mylang = languge;
  }

  getlang(): string {
    return this.mylang;
  }

  applyselectedlang(lang: string) {
    this.setlang(lang);
    this.translate.use(this.getlang());

    if (this.statsService.iSessionStarted()) {
      this.statsService.sendStat({ name: 'SELECT_LANG', value: lang, type: 'text' });
    }
  }

  setCurrentModule(module: any) {
    // Ignore if we accessed the same module
    if (this.currentModule === module) {
      return;
    }

    // Report the unselection of the previous module
    if (this.currentModule) {
      this.statsService.sendStat({
        name: 'UNSELECT_MODULE',
        value: JSON.stringify({
          value: {
            id: this.currentModule.id,
            name: 'Module ' + this.currentModule.id,
            haveChildren: this.currentModule.havesub
          },
          lang: this.getlang()
        }),
        type: 'json'
      });
    }

    this.currentModule = module;

    if (module) {
      // Report the selection of the current module
      this.statsService.sendStat({
        name: 'SELECT_MODULE',
        value: JSON.stringify({
          value: {
            id: this.currentModule.id,
            name: 'Module ' + this.currentModule.id,
            haveChildren: this.currentModule.havesub
          },
          lang: this.getlang()
        }),
        type: 'json'
      });
    }
  }

  setCurrentSubModule(submodule: any) {
    // Ignore if we accessed the same submodule
    if (this.currentSubModule === submodule) {
      return;
    }

    // Report the unselection of the previous submodule
    if (this.currentSubModule) {
      this.statsService.sendStat({
        name: 'UNSELECT_SUB_MODULE',
        value: JSON.stringify({
          value: {
            id: this.currentSubModule.id,
            // name: 'Sous module ' + this.currentSubModule.id,
            // module: this.currentModule.id
          },
          lang: this.getlang()
        }),
        type: 'json'
      });
    }

    this.currentSubModule = submodule;
  }

  setCurrentSection(section: string) {
    // Report the unselection of the previous section
    if (this.currentSection) {
      this.statsService.sendStat({
        name: 'UNSELECT_SECTION',
        value: this.currentSection,
        type: 'text'
      });
    }

    this.currentSection = section;

    if (section) {
      // Report the selection of the current section
      this.statsService.sendStat({
        name: 'SELECT_SECTION',
        value: this.currentSection,
        type: 'text'
      });
    }
  }

  setCurrentVisitType(visit: string) {
    // Report the unselection of the previous section
    if (this.currentVisit) {
      this.statsService.sendStat({
        name: 'UNSELECT_VISIT',
        value: this.currentVisit,
        type: 'text'
      });
    }

    this.currentVisit = visit;

    if (visit) {
      // Report the selection of the current section
      this.statsService.sendStat({
        name: 'SELECT_VISIT',
        value: this.currentVisit,
        type: 'text'
      });
    }
  }

  setCurrentAudio(currentAudio : any ) {
    this.currentAudio = currentAudio;
    // Only report the listened audio if it's not reported yet
    if (
      this.currentAudio &&
      this.currentAudio.lang === this.getlang() &&
      (!this.currentAudio.subModule ||
        (this.currentSubModule && this.currentAudio.subModule === this.currentSubModule.id))
        ) {
          return;
        }

        // console.log(this.currentSubModule);

        this.currentAudio = {
          value: 'Audio (' + this.getlang() + ')',
          lang: this.getlang(),
          // module: this.currentSubModule.id,
        };

        this.statsService.sendStat({
          name: 'LISTEN_AUDIO',
          value: JSON.stringify(this.currentAudio),
          type: 'json'
        });
      }

      startSession(source, id) {
        this.statsService.startSession(source, id);
      }

      GetModules() {
        return this.Modules;
      }

      GetModule(ModulePos: number) {
        return this.Modules[ModulePos - 1];
      }

      // GetSubModule(id: number) {
      //   return this.Modules[id].sub;
      // }

      GetModuleByQrCode(qrcode: string) {
        return this.map[qrcode] ;
      }

      GetLastSubModuleOrder() {
        return this.ModulesWithAudio[this.ModulesWithAudio.length - 1].order;
      }

      GetOrderByModuleId(id : string){
        return this.ModulesWithAudio.find(v => v.element.id == id).order;
      }
    }
