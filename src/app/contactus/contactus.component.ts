import { Title } from "@angular/platform-browser";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { GlobalService } from "../global.service";
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { environment } from "src/environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Component({
  selector: "app-contactus",
  templateUrl: "./contactus.component.html",
  styleUrls: ["./contactus.component.css"]
})
export class ContactusComponent implements OnInit, OnDestroy {
  language: string = this.globals.getlang();
  successMessage: string;

  contactInfos = {
    fullname: "",
    email: "",
    subject: "",
    content: ""
  };

  constructor(
    private http: HttpClient,
    private globals: GlobalService,
    private router: Router,
    private titleService: Title
  ) {}

  contact(contactForm: NgForm) {
    console.log(contactForm);
    console.log(this.contactInfos);

    // The form is invalid, so no need to proceed
    if (contactForm.invalid) {
      return false;
    }

    // Ignore in development
    if (!environment.production) {
      contactForm.resetForm();
      this.contactInfos = {
        fullname: "",
        email: "",
        subject: "",
        content: ""
      };

      this.successMessage = "contact.success";
      setTimeout(() => this.router.navigateByUrl("/thanks"), 2500);
      return false;
    }

    const headers = new HttpHeaders();
    headers.set("Content-Type", "text/plain; charset=utf-8");

    this.http
      .post("/contactus", this.contactInfos, {
        headers,
        responseType: "text"
      })
      .subscribe({
        next: () => {
          contactForm.resetForm();
          this.contactInfos = {
            fullname: "",
            email: "",
            subject: "",
            content: ""
          };

          this.successMessage = "contact.success";
          setTimeout(() => this.router.navigateByUrl("/thanks"), 2500);
        },
        error: error => console.error(error)
      });
  }

  ngOnInit() {
    this.titleService.setTitle("Contact Us Page ");

    this.globals.setCurrentSection("CONTACTEZ-NOUS");
  }

  ngOnDestroy() {
    this.globals.setCurrentSection("");
  }
}
