import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WsScanService {
  private socket;
  public responses = new Subject<string>();

  constructor() {
    const basePath = document.getElementsByTagName('base')[0].getAttribute('href');
    const socketio = (window as any).io;

    if (socketio) {
      this.socket = socketio(location.origin, { path: `${basePath}socket.io` });

      this.socket.on('invalid', () => this.responses.next('invalid'));
      this.socket.on('valid', () => this.responses.next('valid'));
    }
  }

  emitScan(qrcode) {
    console.log('QR Code scanned:', qrcode);

    if (!environment.production) {
      this.responses.next('valid');
      return;
    }

    if (this.socket) {
      this.socket.emit('scan', qrcode);
    }
  }
}
