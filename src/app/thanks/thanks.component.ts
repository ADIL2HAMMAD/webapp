import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-thanks',
  templateUrl: './thanks.component.html',
  styleUrls: ['./thanks.component.css']
})
export class ThanksComponent implements OnInit {

  language: string = this.globals.getlang(); 

  
  constructor(private globals : GlobalService, private titleService: Title) { }

  ngOnInit() {
    this.titleService.setTitle("Thanks Page");
  }

}
