import { GlobalService } from './global.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ModuleCanActivate implements CanActivate {
  constructor(private router: Router, private global: GlobalService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const moduleId = +route.params.id;
    const subModuleId = +route.params.subid;

    const path = state.root.firstChild.routeConfig.path;

    // Details of a module without submodules
    if (path === 'modules/:id/module-details') {
      const module = this.global.GetModule(moduleId);

      // Module exist
      if (module) {
        this.global.setCurrentModule(module);

        // Have submodule
        // if (module.havesub) {
        //   this.global.setCurrentSubModule(module.sub[0]);

        //   this.router.navigate([
        //     '/modules/',
        //     moduleId,
        //     'sub-module',
        //     module.sub[0].id,
        //     'module-details'
        //   ]);
        //   return false;
        // }

        // Doesn't have submodule
        return true;
      }

      // Module doesn't exist
      this.router.navigateByUrl('/404');
      return false;

      // List of submodules of a module
    } else if (path === 'modules/:id/sub-module') {
      const module = this.global.GetModule(moduleId);

      // Module exist
      if (module) {
        this.global.setCurrentModule(module);

        // Have submodule
        // if (module.havesub) {
        //   return true;
        // }

        // Doesn't have submodule
        // this.router.navigateByUrl('/modules/' + moduleId + '/module-details');
        // return false;
      }

      // Module doesn't exist
      this.router.navigateByUrl('/404');
      return false;

      // Details of a submodule of a module
    } else if (path === 'modules/:id/sub-module/:subid/module-details') {
      const module = this.global.GetModule(moduleId);

      // Module exist
      // if (module) {
      //   this.global.setCurrentModule(module);

      //   // Module have submodules
      //   if (module.havesub) {
      //     const submodule = this.global.GetSubModuleById(moduleId - 1, subModuleId);

      //     // Submodule exist
      //     if (submodule) {
      //       this.global.setCurrentSubModule(submodule);
      //       return true;
      //     }

      //     // Submodule doesn't exist
      //     this.router.navigateByUrl('/404');
      //     return false;
      //   }

      //   // Doesn't have submodules
      //   this.router.navigateByUrl('/modules/' + moduleId + '/module-details');
      //   return false;
      // }

      // Module doesn't exist
      // this.router.navigateByUrl('/404');
      // return false;
    }

    if (path === 'visite-guidee/:order') {
      // Module exist
      if (+route.params.order <= this.global.ModulesWithAudio.length) {
        return true;
      }

      // Module doesn't exist
      this.router.navigateByUrl('/404');
      return false;
    }

    return true;
  }
}
