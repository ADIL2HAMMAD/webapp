import { Title } from '@angular/platform-browser';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { GlobalService } from '../global.service';

@Component({
  selector: 'app-plan-musee',
  templateUrl: './plan-musee.component.html',
  styleUrls: ['./plan-musee.component.css']
})
export class PlanMuseeComponent implements OnInit, OnDestroy {
  constructor(private global: GlobalService, private titleService: Title) {}

  language: string = this.global.getlang();

  ngOnInit() {
    this.titleService.setTitle('Plan Museum Page ');

    this.global.setCurrentSection('PLAN DU MUSÉE');
  }

  ngOnDestroy() {
    this.global.setCurrentSection('');
  }
}
