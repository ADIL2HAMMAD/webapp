import { Title } from '@angular/platform-browser';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalService } from '../global.service';
import { QrcodeReaderComponent } from '../qrcode-reader/qrcode-reader.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-qrcode-collection-scanner',
  templateUrl: './qrcode-collection-scanner.component.html',
  styleUrls: ['./qrcode-collection-scanner.component.css']
})
export class QrcodeCollectionScannerComponent implements OnInit, OnDestroy {
  @ViewChild(QrcodeReaderComponent)
  private qrcodeReader: QrcodeReaderComponent;

  constructor(
    private globals: GlobalService,
    private router: Router,
    private titleService: Title,
    private location: Location
    ) {}

    ngOnInit() {
      this.globals.setCurrentVisitType('Visite scan');
      this.titleService.setTitle('QR Code Scan Page - collection');
    }

    ngOnDestroy() {
      this.globals.setCurrentVisitType('');
    }

    back(){
      this.location.back();
    }


    gotoModule(qrcode) {
      // console.log(`Current visite type from QrcodeCollectionScannerComponent : ${this.globals.getCurrentVisitType()}`);

      const module = this.globals.GetModuleByQrCode(qrcode);
      if (module) {
        this.globals.setCurrentModule(module);

        const ordre = this.globals.GetOrderByModuleId("" + module.idModule)

        this.router.navigateByUrl('/visite-guidee/' + ordre );
      } else {
        this.qrcodeReader.reload();
      }
    }
  }
