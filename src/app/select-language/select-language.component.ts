import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';
import { Router } from '@angular/router';
import { PreviousRouteService } from '../previous-route.service';
import { SessionService } from '../session.service';

@Component({
  selector: 'app-select-language',
  templateUrl: './select-language.component.html',
  styleUrls: ['./select-language.component.css']
})
export class SelectLanguageComponent implements OnInit {
  constructor(
    private globals: GlobalService,
    private router: Router,
    private titleService: Title,
    private previousRouteService: PreviousRouteService,
    private sessionService: SessionService
  ) {}

  language: string = this.globals.getlang();

  UseLanguage(lang: string) {
    this.globals.applyselectedlang(lang);
    this.language = this.globals.getlang();
  }

  validate() {
    if (this.sessionService.isLoggedIn()) {
      this.router.navigateByUrl(this.previousRouteService.getPreviousUrl());
    } else {
      this.router.navigateByUrl('/auth');
    }
  }

  ngOnInit() {
    this.titleService.setTitle('Select Language Page');
  }
}
