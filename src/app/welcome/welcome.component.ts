import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';

import { GlobalService } from '../global.service';
import { StatsService } from '../stats.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  language: string = this.global.getlang();


  constructor(
    private titleService: Title,
    private statsService: StatsService,
    private global: GlobalService
  ) {}

  ngOnInit() {
    this.titleService.setTitle('Welcome Page');
    this.statsService.sendStat({ name: 'SELECT_LANG', value: this.global.getlang(), type: 'text' });
  }
}
