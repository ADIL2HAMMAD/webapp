import { Injectable } from '@angular/core';

import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  isLoggedIn(): boolean {
    return Boolean(window.localStorage['logged-in']);
  }

  getSessionId(): string {
    return window.localStorage.id;
  }

  setLastActive(date: Date) {
    window.localStorage.lastActive = date.toISOString();
  }

  getLastActive(): Date {
    return new Date(window.localStorage.lastActive);
  }

  setState(id) {
    window.localStorage.id = id;
    window.localStorage['logged-in'] = true;
    window.localStorage.lastActive = new Date().toISOString();
  }

  destroyState() {
    window.localStorage.removeItem('id');
    window.localStorage.removeItem('logged-in');
    window.localStorage.removeItem('lastActive');
  }
}
