import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { GlobalService } from '../global.service';
import { StatsService } from '../stats.service';

@Component({
  selector: 'app-select-visite',
  templateUrl: './select-visite.component.html',
  styleUrls: ['./select-visite.component.css']
})
export class SelectVisiteComponent implements OnInit {
  constructor(
    private globals: GlobalService,
    private router: Router,
    private titleService: Title
  ) {}

  language: string = this.globals.getlang();

  gotoVisit(type) {
    this.globals.setCurrentVisitType(type === 'libre' ? 'Visite libre' : 'Visite guidée');
    this.router.navigateByUrl(type === 'libre' ? '/modules' : '/visite-guidee/1');
  }
 
  ngOnInit() {
    this.globals.setCurrentSubModule(null);
    this.globals.setCurrentModule(null);
    this.globals.setCurrentVisitType('');

    this.titleService.setTitle('Select Visite Page');
  }
  
}
