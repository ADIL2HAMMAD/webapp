import { Title } from '@angular/platform-browser';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { PlyrModule } from 'ngx-plyr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { SelectLanguageComponent } from './select-language/select-language.component';
import { AuthentificationComponent } from './authentification/authentification.component';
import { FormulaireComponent } from './formulaire/formulaire.component';
import { SelectVisiteComponent } from './select-visite/select-visite.component';
import { NavbarComponent } from './navbar/navbar.component';

// import ngx-translate and the http loader
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ContactusComponent } from './contactus/contactus.component';
import { ConditionsUseComponent } from './conditions-use/conditions-use.component';
import { DispositifsComponent } from './Dispositifs/dispositifs.component';
import { PlanMuseeComponent } from './plan-musee/plan-musee.component';
import { ConseilsVisiteursComponent } from './conseils-visiteurs/conseils-visiteurs.component';

import { GlobalService } from './global.service';
import { VisiteLibreComponent } from './visite-libre/visite-libre.component';
import { VisiteGuideeComponent } from './visite-guidee/visite-guidee.component';
import { QrcodeIdentificationScannerComponent } from './qrcode-identification-scanner/qrcode-identification-scanner.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ThanksComponent } from './thanks/thanks.component';

import { QrcodeReaderComponent } from './qrcode-reader/qrcode-reader.component';
import { QrcodeCollectionScannerComponent } from './qrcode-collection-scanner/qrcode-collection-scanner.component';
import { GlossaryLetterListComponent } from './glossary/glossary-letter-list/glossary-letter-list.component';
import { GlossaryLetterWordsComponent } from './glossary/glossary-letter-words/glossary-letter-words.component';
import { GlossaryLetterWordDefinitionComponent } from './glossary/glossary-letter-word-definition/glossary-letter-word-definition.component';
import { DispositifDigitauxDetailsComponent } from './dispositif-digitaux-details/dispositif-digitaux-details.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    SelectLanguageComponent,
    AuthentificationComponent,
    FormulaireComponent,
    SelectVisiteComponent,
    NavbarComponent,
    ContactusComponent,
    ConditionsUseComponent,
    DispositifsComponent,
    PlanMuseeComponent,
    ConseilsVisiteursComponent,
    VisiteLibreComponent,
    VisiteGuideeComponent,
    QrcodeIdentificationScannerComponent,
    NotFoundComponent,
    ThanksComponent,
    QrcodeReaderComponent,
    QrcodeCollectionScannerComponent,
    GlossaryLetterListComponent,
    GlossaryLetterWordsComponent,
    GlossaryLetterWordDefinitionComponent,
    DispositifDigitauxDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    PlyrModule,

    // configure the imports
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],

  providers: [GlobalService, Title],
  bootstrap: [AppComponent]
})
export class AppModule {}

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  // TODO: Note that you can also use `document.baseURI` to acheive the same
  return new TranslateHttpLoader(http, document.baseURI + 'assets/i18n/');
}
