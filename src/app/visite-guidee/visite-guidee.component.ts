import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from './../global.service';
import { Component, ViewChild, AfterViewChecked } from '@angular/core';
import { PlyrComponent } from 'ngx-plyr';

@Component({
  selector: 'app-visite-guidee',
  templateUrl: './visite-guidee.component.html',
  styleUrls: ['./visite-guidee.component.css']
})
export class VisiteGuideeComponent implements AfterViewChecked {
  id: number;
  subid: number;
  isPaused: boolean = null;
  isModule: boolean = null;
  haveAudio: boolean = true;
  isLast: boolean = null;
  isFirst: boolean = null;
  havecomplement: boolean = null;

  language: string = this.globals.getlang();
  order: number;
  imgcoin: string;
  imgactive: string;
  imgprotrait: string;
  AllAudio: {};
  AllModules = this.globals.GetModules();

  audioSources: any;

  options = {
    controls: ['progress'],
    autoplay: false,
    invertTime: false
  };

  // get the component instance to have access to plyr instance
  @ViewChild(PlyrComponent)
  plyr: PlyrComponent;

  byScan = false;

  constructor(
    private globals: GlobalService,
    private router: Router,
    private route: ActivatedRoute,
    private titleService: Title
    ) {
      this.route.params.subscribe(params => {


        this.order = +params.order;
        this.isPaused = true;

        if (this.order  == 1) {
          this.isFirst = true
          this.isLast = false;
        }
        if (this.order == this.globals.GetLastSubModuleOrder()) {
          this.isLast = true;
          this.isFirst = false;
        }
        if (this.order  != 1 && this.order != this.globals.GetLastSubModuleOrder()) {
          this.isFirst = false;
          this.isLast = false;
        }

        this.imgdescs = [];

        (window as any).jQuery('.carousel').carousel(0);

        this.FillData();

      });

    }


    isEmptyObject(obj) {
      return (obj && (Object.keys(obj).length === 0));
    }


    Rewind(isRewind: boolean) {
      if (isRewind) {
        this.plyr.player.currentTime -= 10;
      } else {
        this.plyr.player.currentTime += 10;
      }
    }

    StartPaused() {
      if (this.isPaused) {
        this.globals.setCurrentAudio(this.AllAudio[this.language]);
      }

      this.isPaused = !this.isPaused;
      this.plyr.player.togglePlay();
    }

    GoToNext() {
      this.order += 1;
      if (this.order <= this.AllModules.length) {
        this.router.navigate(['/visite-guidee', this.order]);
      } else {
        this.router.navigate(['/thanks']);
      }

      this.isPaused = true;
    }



    ColorStyleFirst() {
      // Object.assign(this.imgdescs, {i: 'Modules.'+this.id+'.imagedesc.'+[i]});S
      let style = {
        fill: this.isFirst ? '#6c757d' : '#fff',
        cursor: this.isFirst ? 'not-allowed' : 'pointer'
      };
      return style;
    }

    ColorStyleLast() {
      let style = {
        fill: this.isLast ? '#6c757d' : '#fff',
        cursor: this.isLast ? 'not-allowed' : 'pointer'
      };
      return style;
    }

    go(step : number){
      if (step == 1) {
        this.router.navigate(['/visite-guidee', this.order+1]);
      } else{
        this.router.navigate(['/visite-guidee', this.order-1]);

      }
    }

    imagesDetails : {images , descriptions};
    imgcoins : {} ;
    imgdescs = [] ;

    FillData() {
      const CurrentModuleOrder = this.AllModules.find(v => +v.id === this.order);

      this.imgcoins = CurrentModuleOrder.imgcoins;
      this.AllAudio = CurrentModuleOrder.audio;
      this.id = +CurrentModuleOrder.id;
      this.imgprotrait = CurrentModuleOrder.imgprotrait ;
      this.havecomplement = CurrentModuleOrder.havecomplement;


      if (!this.AllAudio) {
        this.haveAudio = false;
        this.audioSources = [{ }];
      }else{
        this.haveAudio = true;
        this.audioSources = [{
          src: this.AllAudio[this.language],
          type: 'audio/mpeg'
        }];
      }


        for (let i = 0; i < Object.keys(this.imgcoins).length; i++) {
          this.imgdescs.push('Modules.'+this.id+'.imagedesc.'+[i + 1])
        }

        this.imagesDetails = {
          images: Object.values(this.imgcoins),
          descriptions: this.imgdescs
        };



      this.globals.setCurrentModule(CurrentModuleOrder);



    }

    ngAfterViewChecked() {
      this.titleService.setTitle(document.getElementById('titleModule').textContent);
    }

    ngOnInit() {
    }
  }
