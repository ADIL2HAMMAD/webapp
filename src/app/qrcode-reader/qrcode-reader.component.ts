import { Component, EventEmitter, Input, Output, OnDestroy, OnInit } from '@angular/core';
import jsQR from 'jsqr';

import { GlobalService } from '../global.service';

enum QR_CODE_TYPE {
  IDENTIFICATION = 'IDENTIFICATION',
  COLLECTION = 'COLLECTION'
}

@Component({
  selector: 'app-qrcode-reader',
  templateUrl: './qrcode-reader.component.html',
  styleUrls: ['./qrcode-reader.component.css']
})
export class QrcodeReaderComponent implements OnDestroy, OnInit {
  private canvasElement: HTMLCanvasElement;
  private videoElement: HTMLVideoElement;
  private videoStream: MediaStream;

  private containerElement: HTMLElement;
  private overlayElement: HTMLElement;

  private rAFId;
  private qrCodePattern;

  // Our resize and visibilitychange event handlers. We need to resort to this
  // trick in order to remove the listeners when the component is destroyed.
  // We don't want the events to be catched in other pages.
  private eventListeners = {
    resize: null,
    visibilitychange: null
  };

  //
  // Destination position/dimension of the video frame to draw on the canvas
  //
  private dx = 0;
  private dy = 0;

  private dHeight: number;
  private dWidth: number;

  //
  // Source position/dimension of the video frame to draw on the canvas
  //
  private sx = 0;
  private sy = 0;

  private sHeight: number;
  private sWidth: number;

  public error;

  @Output('detection')
  private qrCodeDetected = new EventEmitter<string>();

  @Input('type')
  private qrCodeType = QR_CODE_TYPE.IDENTIFICATION;

  currentLang: string;
  showCopyButton: boolean;

  copyURL: () => void;

  constructor(private globalService: GlobalService) {
    this.currentLang = this.globalService.getlang();
  }

  ngOnInit() {
    this.canvasElement = document.querySelector('#qrcode-reader-target');
    this.videoElement = document.querySelector('#qrcode-reader-source');

    this.containerElement = document.querySelector('.qrcode-reader-container');
    this.overlayElement = document.querySelector('.qrcode-reader-overlay');

    // Resize the canvas and the overlay
    this.resize();

    this.qrCodePattern =
      this.qrCodeType === QR_CODE_TYPE.IDENTIFICATION
        ? /^https:\/\/192\.168\.1\.200\/identify\/([a-zA-Z0-9]{10})$/
        //? /^https:\/\/webapp\.bkam\.ma\/identify\/([a-zA-Z0-9]{10})$/
        : /^BKAM:([a-zA-Z0-9]{10}):COLLECTION$/;

    // Start detecting QR codes
    this.start();

    // Installing our event listeners
    this.eventListeners.resize = () => {
      this.resize();
    };

    this.eventListeners.visibilitychange = () => {
      // When using the web cam, we need to turn it off when we aren't using it
      if (document.visibilityState === 'hidden' && this.videoStream) {
        this.stop();
      } else {
        this.start();
      }
    };

    document.addEventListener('visibilitychange', this.eventListeners.visibilitychange);

    window.addEventListener('resize', this.eventListeners.resize);
  }

  private start() {
    this.requestCamera()
      .then(stream => this.handleStream(stream))
      .catch(error => this.handleError(error));
  }

  reload() {
    this.videoElement.play();
    this.videoElement.classList.remove('flash');

    this.rAFId = requestAnimationFrame(() =>
      this.drawOnCanvas(this.canvasElement.getContext('2d'))
    );
  }

  private stop() {
    cancelAnimationFrame(this.rAFId);

    // Stop reading from the webcam
    this.videoStream.getTracks().forEach(track => track.stop());
  }

  private requestCamera() {
    const isIOS = /iPad|iPhone|iPod/.test(navigator.userAgent);
    const isSafari = /Version\/[\d\.]+.*Safari/.test(navigator.userAgent);

    // The user is using another browser on IOS. They should use Safari instead
    // as it's the only one who have access to webcam
    if (isIOS && !isSafari && (!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia)) {
      const err = new Error('Please use Safari to view this reader.');
      err.name = 'BROWSER_DO_NOT_SUPPORT_MEDIA_DEVICE';

      this.showCopyButton = true;

      this.copyURL = () => {
        const el: HTMLTextAreaElement = document.querySelector('#url-text');
        const tooltip = document.querySelector('.tooltiptext');

        el.value = location.href;

        const range = document.createRange();
        el.contentEditable = 'true';
        el.readOnly = true;

        range.selectNodeContents(el);

        const selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);

        el.setSelectionRange(0, 999999);

        document.execCommand('copy');

        tooltip.classList.add('show');
        setTimeout(() => tooltip.classList.remove('show'), 1500);
      };

      return Promise.reject(err);
    }

    return navigator.mediaDevices.getUserMedia({
      video: {
        facingMode: 'environment',
        width: { min: 640, ideal: 1280, max: 1920 },
        height: { min: 480, ideal: 720, max: 1080 }
      },
      audio: false
    });
  }

  private normalizeValue(value: string) {
    if (this.qrCodeType === QR_CODE_TYPE.IDENTIFICATION) {
      try {
        const url = new URL(value);

        if (this.qrCodePattern.test(url.href)) {
          return url.href;
        }
      } catch (err) {
        return;
      }
    }

    if (this.qrCodeType === QR_CODE_TYPE.COLLECTION) {
      if (value.length !== 26) {
        return;
      }

      if (this.qrCodePattern.test(value)) {
        return value;
      }
    }

    return;
  }

  private drawOnCanvas(context: CanvasRenderingContext2D) {
    // Draw only the part of the video that is highlighted by the overly
    context.drawImage(
      this.videoElement,
      this.sx,
      this.sy,
      this.sWidth,
      this.sHeight,
      this.dx,
      this.dy,
      this.dWidth,
      this.dHeight
    );

    const imageData = context.getImageData(
      0,
      0,
      this.canvasElement.width,
      this.canvasElement.height
    );

    const code = jsQR(imageData.data, imageData.width, imageData.height, {
      inversionAttempts: 'dontInvert'
    });

    if (code) {
      const qrCodeValue = this.normalizeValue(code.data);

      if (qrCodeValue) {
        this.videoElement.pause();

        // Flash the screen :D
        this.videoElement.classList.add('flash');

        const value = this.qrCodePattern.exec(qrCodeValue)[1];

        // Report the detected QR code to the caller component
        setTimeout(() => this.qrCodeDetected.emit(value), 250);
        return;
      }
    }

    this.rAFId = requestAnimationFrame(() => this.drawOnCanvas(context));
  }

  private handleStream(stream: MediaStream) {
    this.videoStream = stream;

    this.videoElement.srcObject = stream;

    this.videoElement.onloadedmetadata = () => {
      // required to tell iOS safari we don't want fullscreen
      this.videoElement.setAttribute('playsinline', 'true');
      this.videoElement.play().catch(err => {
        console.dir(err);
      });
    };

    this.videoElement.onplay = () => {
      this.resize();
      this.drawOnCanvas(this.canvasElement.getContext('2d'));
    };
  }

  private handleError(error: any) {
    if (error.name === 'NotFoundError') {
      this.error = 'qrcodereader.errors.notfound';
    } else if (error.name === 'BROWSER_DO_NOT_SUPPORT_MEDIA_DEVICE') {
      this.error = 'qrcodereader.errors.nosupport';
    } else {
      // error.name === 'NotAllowedError'
      this.error = 'qrcodereader.errors.default';
    }
  }

  private resize() {
    const width = this.containerElement.offsetWidth;
    const height = this.containerElement.offsetHeight;

    const videoDimensions = {
      width: this.videoElement.videoWidth,
      height: this.videoElement.videoHeight
    };

    const heightRatio = videoDimensions.height / height;
    const widthRatio = videoDimensions.width / width;

    let scaleFactor = 1 / Math.min(heightRatio, widthRatio);
    scaleFactor = Number.isFinite(scaleFactor) ? scaleFactor : 1;

    this.videoElement.style.transform = 'translate(-50%, -50%) scale(' + scaleFactor + ')';

    const overlayDimensions = getOverlayDimensions(width, height);

    //
    // These properties will be used to draw the covered zone into the canvas.
    // We need to know the start of the data to draw and its size. The
    // destination is the canvas that we already know its size
    //

    // Getting the height/width of the covered aria in the video.
    this.dHeight = this.dWidth = overlayDimensions.width / scaleFactor;

    // Set the canvas' dimensions to the width of the covered zone
    this.canvasElement.width = this.canvasElement.height = this.dWidth;

    // Getting the coordinates of the top-left point of the covered zone of the
    // video
    this.sx = videoDimensions.width / 2 - this.dWidth / 2;
    this.sy = videoDimensions.height / 2 - this.dHeight / 2;

    // The width and the height of the source is the same as the ones of the
    // canvas
    this.sWidth = this.dWidth;
    this.sHeight = this.dHeight;

    // Resize the overlay by setting the new width of each border
    const boxPaddingHeightSize = overlayDimensions.paddingHeight;
    const boxPaddingWidthSize = overlayDimensions.paddingWidth;
    this.overlayElement.style.borderLeftWidth = boxPaddingWidthSize + 'px';
    this.overlayElement.style.borderTopWidth = boxPaddingHeightSize + 'px';
    this.overlayElement.style.borderRightWidth = boxPaddingWidthSize + 'px';
    this.overlayElement.style.borderBottomWidth = boxPaddingHeightSize + 'px';

    function getOverlayDimensions(width, height) {
      const minLength = Math.min(width, height);
      const paddingHeight = (height + 64 - minLength) / 2;
      const paddingWidth = (width + 64 - minLength) / 2;

      return {
        paddingHeight,
        paddingWidth,
        width: minLength - 64,
        height: minLength - 64
      };
    }
  }

  ngOnDestroy() {
    if (!this.videoStream) {
      return;
    }

    this.stop();

    // Remove our pre-installed event listeners
    document.removeEventListener('visibilitychange', this.eventListeners.visibilitychange);
    window.removeEventListener('resize', this.eventListeners.resize);
  }
}
