import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { SessionService } from './session.service';

@Injectable({
  providedIn: 'root'
})
export class StatsService {
  private socket;
  private sessionSource: string;
  private sessionStarted = false;

  constructor(private sessionService: SessionService, private router: Router) {
    const basePath = document.getElementsByTagName('base')[0].getAttribute('href');
    const socketio = (window as any).io;

    if (socketio) {
      this.socket = socketio(location.origin, { path: `${basePath}socket.io` });

      if (this.sessionService.isLoggedIn()) {
        this.socket.sessionId = this.sessionService.getSessionId();
        this.sessionStarted = true;
      } else {
        this.socket.on('sessionCreated', sessionId => {
          this.socket.sessionId = sessionId;
          this.sessionStarted = true;

          // Save the session to localStorage
          this.sessionService.setState(sessionId);

          // Report the source of the identification
          this.sendStat({ name: 'IDENTIFICATION', value: this.sessionSource, type: 'text' });

          // Head to the welcome page
          this.router.navigateByUrl('/welcome');
        });
      }
    }
  }

  iSessionStarted(): boolean {
    return this.sessionStarted;
  }

  startSession(source, id) {
    // We imitate the loggin state if we are in development mode.
    if (!this.socket) {
      console.log('startSession:', id);

      this.sessionStarted = true;
      this.sessionService.setState(id);
      this.router.navigateByUrl('/welcome');
      return;
    }

    this.sessionSource = source;
    this.socket.emit('startSession', id);
  }

  endSession() {
    // We imitate the logout state if we are in development mode.
    if (!this.socket || !this.sessionStarted) {
      console.log('endSession');

      this.sessionStarted = false;
      this.sessionService.destroyState();
      return;
    }

    this.socket.emit('endSession', this.socket.sessionId);
    this.socket.sessionId = undefined;
    this.sessionStarted = false;

    // Remove the session from localStorage
    this.sessionService.destroyState();
  }

  sendStat(stat) {
    if (!this.socket || !this.sessionStarted) {
      console.log('stats', stat);
      return;
    }

    stat.sessionId = this.socket.sessionId;

    this.socket.emit('stats', stat);
  }
}
